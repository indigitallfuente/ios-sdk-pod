//
//  RequestEvent.h
//  indigitall-objc
//
//  Created by indigitall on 04/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INPush.h"
#import "INBaseRequest.h"
#import "INPermissions.h"
NS_ASSUME_NONNULL_BEGIN

@interface INRequestEvent : INBaseRequest

typedef enum{
    _CLICK,
    _RECEIVE,
    _SEND,
    _DISMISS
}INEventType;

typedef enum{
    _ACCEPT,
    _REJECT,
    _ASK
}INEventTypePermission;

typedef enum{
    _UPDATE
}INEventTypeLocation;

//typedef enum{
//    push,
//    location
//}PermissionType;

@property (nonatomic)NSString *rawValue;

@property (nonatomic) INPush *push;
@property (nonatomic) INEventType *eventType;
@property (nonatomic) int clickedButton;

@property (nonatomic) NSString * INEventTypePermission;
@property (nonatomic) NSString * permissionType;

@property (nonatomic) INEventTypeLocation *INEventTypeLocation;
@property (nonatomic) double latitude;
@property (nonatomic) double longitud;
@property (nonatomic) NSString *deviceId;
@property (nonatomic) NSString *externalId;

@property (nonatomic) NSString *customEvent;
@property (nonatomic) NSDictionary *customEventData;
@property (nonatomic, nullable) NSString *networkEvent;
@property (nonatomic) BOOL networkEventsEnabled;
@property (nonatomic) int applicationId;
@property (nonatomic) BOOL isPushSecure;

- (id) init;

- (id) initWithPush: (INPush *)push event: (INEventType)event clickedButton:(int)clickedButton;
- (id) requestEventPush;
//
- (id) requestEventVisit;
//
-(id) initWithPermission: (INEventTypePermission )event permission:(INPermissionType )permission;
-(id) initWithStringPermission: (NSString *)INEventTypePermission permission:(NSString *)permissionType;
- (id) requestEventPermission;

-(id) initWithLocation: (INEventTypeLocation)event lat:(double)latitude long:(double)longitud;
- (id) requestEventLocation;

- (id) initWithSendCustomEvent: (NSString *) customEvent customData:(NSDictionary *)customData;
- (id) requestEventCustom;

//var event is useless but is need to Xamarin
- (id) initWithNetworkEvent: (NSString *) networkEvent event:(Boolean)event;
- (id) requestNetworkEvent;

- (id) initWithPushEvent: (INPush *)push isPushSecure: (BOOL) isPushSecure;
- (id) requestEventPushReceived;

@end

NS_ASSUME_NONNULL_END
