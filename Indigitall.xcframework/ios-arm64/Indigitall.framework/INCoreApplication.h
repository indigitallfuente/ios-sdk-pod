//
//  INCoreApplication.h
//  IndigitallSdk
//
//  Created by indigitall on 13/2/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCoreApplication : NSObject

@property (nonatomic) bool contentAvailable;
@property (nonatomic) int locationAccuracy;
@property (nonatomic) int locationDistance;
@property (nonatomic) bool locationEnabled;
@property (nonatomic) int locationUpdateMinutes;
@property (nonatomic) NSString* maintenanceWindowStart;
@property (nonatomic) NSString* maintenanceWindowEnd;
@property (nonatomic) bool networkEventsEnabled;
@property (nonatomic) int networkUpdateMinutes;
@property (nonatomic) int serviceSyncTime;
- (id)init: (NSDictionary *) json;
- (id)init: (NSDictionary *) json fromCache:(bool)isFromCache;

@end

NS_ASSUME_NONNULL_END
