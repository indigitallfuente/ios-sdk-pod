//
//  INCustomerErrorCode.h
//  Indigitall
//
//  Created by indigitall on 23/5/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

typedef enum {
    CUSTOMER_GENERAL_ERROR = 4600,
    CUSTOMER_NO_EXTERNAL_CODE = 4601,
} INCustomerErrorCode;

extern NSString* _Nonnull const FormatStateCustomerError_toString[];

NS_ASSUME_NONNULL_END
