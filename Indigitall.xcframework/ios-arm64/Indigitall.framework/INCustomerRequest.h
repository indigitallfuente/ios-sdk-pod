//
//  CustomerRequest.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import "INBaseRequest.h"
#import <Foundation/Foundation.h>
#import "INChannel.h"

NS_ASSUME_NONNULL_BEGIN

@interface INCustomerRequest : INBaseRequest

@property (nonatomic) NSString *customerId;
@property (nonatomic) NSDictionary *fields;
@property (nonatomic) NSArray<NSString *> *fieldNames;
@property (nonatomic) NSString *link;
@property (nonatomic) INChannel channel;
@property (nonatomic) NSString *deviceId;

/// customer journey
@property (nonatomic) NSString *eventType;
@property (nonatomic) NSDictionary *customData;

- (id) init;
- (id) getCustomerRequest;
- (id) getCustomerFieldRequest;
- (id) putCustomerFieldRequest;
- (id) deleteCutomerFieldRequest;
- (id) postAndDeleteCustomerLinkRequest;
- (id) postCustomEventRequest;

@end

NS_ASSUME_NONNULL_END
