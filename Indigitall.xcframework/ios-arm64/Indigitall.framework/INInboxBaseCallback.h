//
//  InboxBaseCallback.h
//  Indigitall
//
//  Created by indigitall on 23/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseCallback.h"


NS_ASSUME_NONNULL_BEGIN

@interface INInboxBaseCallback : INBaseCallback

typedef void(^onErrorInboxBase)(INError * error);
@property (readwrite, copy) onErrorInboxBase onError;

typedef void(^onSuccessInboxBase)(void);
@property (readwrite, copy) onSuccessInboxBase onSuccess;

- (id) initWithOnSuccess: (void(^)(void))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;

@end

NS_ASSUME_NONNULL_END
