//
//  InboxStatus.h
//  Indigitall
//
//  Created by indigitall on 08/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInboxStatus : NSObject
@property (nonatomic, readonly, class) NSString * SENT;
@property (nonatomic, readonly, class) NSString * CLICK;
@property (nonatomic, readonly, class) NSString * DELETED;

+ (NSString *) status: (INInboxStatus *)inboxStatus;
@end

typedef enum {
    SENT,
    CLICK,
    DELETED
} INInboxMessageStatus;

extern NSString* _Nonnull const FormatStateInboxMessageStatus_toString[];

NS_ASSUME_NONNULL_END
