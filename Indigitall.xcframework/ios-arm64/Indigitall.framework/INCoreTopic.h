//
//  INCoreTopic.h
//  Indigitall
//
//  Created by indigitall on 29/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INChannel.h"
NS_ASSUME_NONNULL_BEGIN

@interface INCoreTopic : NSObject
// MARK: - Properties
/// A string containing the topics's code
@property (nonatomic) NSString *code;
/// A string containing the topics's name
@property (nonatomic) NSString *name;
/// A string containing the button's parent code
@property (nonatomic) NSString *parentCode;
/// A bool that inidicates if the action is visible
@property (nonatomic) BOOL visible;
/// A bool that inidicates if the user is subscribed
@property (nonatomic) BOOL subscribed;
/// channel
@property (nonatomic) INChannel channel;

- (id) init: (NSDictionary *)json;
- (id) initWithCode: (NSString *)code name:(NSString *)name parentCode:(NSString *)parentCode visible:(BOOL)visible subscribed:(BOOL)subscribed channel: (INChannel)channel;
@end

NS_ASSUME_NONNULL_END
