//
//  InAppFormUtils.h
//  IndigitallInApp
//
//  Created by indigitall on 27/12/22.
//

#import <Foundation/Foundation.h>
#import "INInAppError.h"
#import "INInAppForm.h"
#import "INEventUtils.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppFormUtils : NSObject

+ (NSArray<INInAppError *>*) checkFieldsForm: (NSDictionary *)forms withInApp:(INInApp *)inApp;
+ (NSString *)editingFormHtml: (NSString *)html script:(NSString *)script;
@end

NS_ASSUME_NONNULL_END
