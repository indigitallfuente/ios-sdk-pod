//
//  InAppSchema.h
//  Indigitall
//
//  Created by indigitall on 10/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppSchema : NSObject

@property (nonatomic, class, readonly) NSString* INAPP_TABLE;
@property (nonatomic, class, readonly) NSString* COLUMN_CREATED_AT;
@property (nonatomic, class, readonly) NSString* COLUMN_UPDATED_AT;
@property (nonatomic, class, readonly) NSString* COLUMN_INAPP_ID;
@property (nonatomic, class, readonly) NSString* COLUMN_LAST_VERSION_ID;
@property (nonatomic, class, readonly) NSString* COLUMN_SHOW_ONCE;
@property (nonatomic, class, readonly) NSString* COLUMN_RENEWAL_TIME;
@property (nonatomic, class, readonly) NSString* COLUMN_CREATION_DATE;
@property (nonatomic, class, readonly) NSString* COLUMN_EXPIRED_DATE;
@property (nonatomic, class, readonly) NSString* COLUMN_INAPP_SHOW_TIMES_SHOWED;
@property (nonatomic, class, readonly) NSString* COLUMN_INAPP_SHOW_TIMES_CLICKED;
@property (nonatomic, class, readonly) NSString* COLUMN_PROPERTIES_CONTENT_URL;
@property (nonatomic, class, readonly) NSString* COLUMN_PROPERTIES_SHOWTIME;
@property (nonatomic, class, readonly) NSString* COLUMN_PROPERTIES_NUMBER_OF_SHOWS;
@property (nonatomic, class, readonly) NSString* COLUMN_PROPERTIES_NUMBER_OF_CLICKS;
@property (nonatomic, class, readonly) NSString* COLUMN_PROPERTIES_DISMISS_FOREVER;
@property (nonatomic, class, readonly) NSString* COLUMN_SCHEMA_CODE;
@property (nonatomic, class, readonly) NSString* COLUMN_SCHEMA_WIDTH;
@property (nonatomic, class, readonly) NSString* COLUMN_SCHEMA_HEIGHT;
@property (nonatomic, class, readonly) NSString* COLUMN_WAS_DISMISSED;
@property (nonatomic, class, readonly) NSString* COLUMN_INAPP_LAYOUT_BORDER_RADIOUS;
@property (nonatomic, class, readonly) NSString* COLUM_INAPP_VERSION;
@property (nonatomic, class, readonly) NSString* COLUM_INAPP_CUSTOM_DATA;
@property (nonatomic, class, readonly) NSString* COLUMN_FILTERS;
@property (nonatomic, class, readonly) NSString* COLUMN_NAME;

@property (nonatomic, class, readonly) NSString* INAPP_TABLE_CREATE;
@property (nonatomic, class, readonly) NSArray* INAPP_COLUMNS;

+ (NSArray<NSString *>*) upgradeDatabase: (int)version;
@end

NS_ASSUME_NONNULL_END
