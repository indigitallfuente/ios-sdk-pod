//
//  INTopic.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INAPIConstants.h"
#import "INCoreTopic.h"
NS_ASSUME_NONNULL_BEGIN

@interface INTopic : INCoreTopic

- (id) init: (NSDictionary *)json;
- (id) initWithCode: (NSString *)code name:(NSString *)name parentCode:(NSString *)parentCode visible:(BOOL)visible subscribed:(BOOL)subscribed channel: (INChannel)channel;
@end

NS_ASSUME_NONNULL_END
