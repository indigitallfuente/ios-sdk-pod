//
//  INInboxDefaults.h
//  Indigitall
//
//  Created by indigitall on 23/5/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INDefaults.h"
NS_ASSUME_NONNULL_BEGIN

@interface INInboxDefaults : INDefaults

@property (nonatomic)NSString *authToken;
@property (nonatomic)NSString *inboxLastAccess;

+ (void) setInboxLastAccess:(NSString *)lastAccess;
+ (NSString *) getInboxLastAccess;

+ (void) setAuthToken:(NSString *)authToken;
+ (NSString *) getAuthToken;



@end

NS_ASSUME_NONNULL_END
