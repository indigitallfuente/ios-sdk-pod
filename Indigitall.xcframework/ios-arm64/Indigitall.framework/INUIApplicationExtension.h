//
//  UIApplicationExtension.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface INUIApplicationExtension : NSObject
- (UIViewController *)topViewController: (UIViewController *)controller;
- (UIViewController *)mostTopViewController;

@end

NS_ASSUME_NONNULL_END
