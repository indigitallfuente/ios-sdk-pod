//
//  InAppLocationHandler.h
//  Indigitall
//
//  Created by indigitall on 21/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INCoreLocationHandler.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppLocationHandler : INCoreLocationHandler

@end

NS_ASSUME_NONNULL_END
