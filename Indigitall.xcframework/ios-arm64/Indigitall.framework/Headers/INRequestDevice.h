//
//  RequestDevice.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseRequest.h"
#import "INDevice.h"
#import "INTopic.h"

NS_ASSUME_NONNULL_BEGIN
@interface INRequestDevice : INBaseRequest

@property (nonatomic, readonly) NSString * PARAMS_VERSION;
@property (nonatomic, readonly) NSString * PARAMS_OSNAME;
@property (nonatomic, readonly) NSString * PARAMS_OSVERSION;
@property (nonatomic, readonly) NSString * PARAMS_DEVICEBRAND;
@property (nonatomic, readonly) NSString * PARAMS_DEVICEMODEL;
@property (nonatomic, readonly) NSString * PARAMS_CARRIER;
@property (nonatomic, readonly) NSString * PARAMS_DEVICETYPE;
@property (nonatomic, readonly) NSString * PARAMS_APPVERSION;
@property (nonatomic, readonly) NSString * PARAMS_LOCALE;
@property (nonatomic, readonly) NSString * PARAMS_TIMEZONE;
@property (nonatomic, readonly) NSString * PARAMS_TIMEOFFSET;
@property (nonatomic, readonly) NSString * PARAMS_PRODUCTNAME;
@property (nonatomic, readonly) NSString * PARAMS_PRODUCTVERSION;
@property (nonatomic, readonly) NSString * PARAMS_EXTERNALCODE;
@property (nonatomic, readonly) NSString * PARAMS_ENABLED;
@property (nonatomic, readonly) NSString * PARAMS_EXTERNAL_APPS;


@property (nonatomic) INDevice *device;

- (id) init: (INDevice *)device;

- (id) getDeviceRequest;
- (id) postDeviceRequest;
- (id) putDeviceRequest;
- (id) updateRequest: (BOOL )status;

- (NSMutableDictionary *) getBodyConstants;

@property (nonatomic) NSArray<NSString *> *topics;

- (id)initWithTopics:(NSArray<NSString *> *)topics;
- (id) getTopicsRequest;
- (id) deleteTopicsRequest;
- (id) postTopicsRequest;

- (NSMutableDictionary *)getBodyTopics;


@end

NS_ASSUME_NONNULL_END
