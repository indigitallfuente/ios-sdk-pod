//
//  INPushObserverUtils.h
//  IndigitallSdk
//
//  Created by indigitall on 29/1/25.
//

#import <Foundation/Foundation.h>

#import "INPermissions.h"
#import "INDevice.h"
#import "INPushErrorCode.h"
#import "INObserverUtils.h"
#import "INError.h"

NS_ASSUME_NONNULL_BEGIN

@interface INPushObserverUtils : NSObject
+ (void) isOnForegroundWithTag: (NSString *)TAG fromMethod: (NSString *)method  withCompletion: (void(^)(BOOL isOnForeground)) completion;
+ (void) isOnForegroundWithTag: (NSString *)TAG
                    fromMethod: (NSString *)method
   withOnIndigitallInitialized: (void(^)(NSArray<INPermissions*> *permissions, INDevice *device)) callback
        withOnErrorInitialized:(void(^)(INError *onError))onError
                withCompletion: (void(^)(BOOL isOnForeground)) completion;
+ (void) isOnForegroundWithTag: (NSString *)TAG
                    fromMethod: (NSString *)method
                 withOnSuccess: (void(^)(INDevice *device))onSuccess
                       onError:(void(^)(INError *error))onError
                withCompletion: (void(^)(BOOL isOnForeground)) completion;
@end

NS_ASSUME_NONNULL_END
