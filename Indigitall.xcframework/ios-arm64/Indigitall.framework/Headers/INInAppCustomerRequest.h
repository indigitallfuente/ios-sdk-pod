//
//  InAppCustomerRequest.h
//  IndigitallInApp
//
//  Created by indigitall on 27/12/22.
//

#import "INBaseRequest.h"
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppCustomerRequest : INBaseRequest
@property (nonatomic) NSString *customerId;
@property (nonatomic) NSDictionary *fields;
@property (nonatomic) NSString *deviceId;

- (id) init;
- (id) putCustomerFieldRequest;


@end

NS_ASSUME_NONNULL_END
