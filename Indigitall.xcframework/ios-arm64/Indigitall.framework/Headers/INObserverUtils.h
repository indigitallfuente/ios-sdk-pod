//
//  INObserverUtils.h
//  IndigitallSdk
//
//  Created by indigitall on 29/1/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INObserverUtils : NSObject
+ (void) isNotOnForegroundWithTag: (NSString *)TAG fromMethod: (NSString *)method withCompletion: (void(^)(BOOL isbackground)) completion;

@end

NS_ASSUME_NONNULL_END
