//
//  InboxCountersCallback.h
//  Indigitall
//
//  Created by indigitall on 07/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "INBaseCallback.h"
#import "INInboxCounters.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxCountersCallback : INBaseCallback

@property (nonatomic) INInboxCounters *counter;

typedef void(^onErrorInboxCounter)(INError * error);
@property (readwrite, copy) onErrorInboxCounter onError;

typedef void(^onSuccessInboxCounter)(INInboxCounters * counter);
@property (readwrite, copy) onSuccessInboxCounter onSuccess;

- (id)initWithOnSuccess: (void(^)(INInboxCounters *))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;


@end

NS_ASSUME_NONNULL_END
