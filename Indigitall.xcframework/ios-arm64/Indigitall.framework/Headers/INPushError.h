//
//  INPushError.h
//  Indigitall
//
//  Created by indigitall on 23/5/22.
//  Copyright © 2022 Indigital. All rights reserved.
//
 
#import <Foundation/Foundation.h>
#import "INError.h"
#import "INPushErrorCode.h"

NS_ASSUME_NONNULL_BEGIN

@interface INPushError : INError

@property (readwrite, nonatomic) INPushErrorCode pushErrorCode;

- (id)initWithErrorCode:(INPushErrorCode)errorCode descriptionMessage:(nullable NSString *)descriptionMessage;

@end

NS_ASSUME_NONNULL_END
