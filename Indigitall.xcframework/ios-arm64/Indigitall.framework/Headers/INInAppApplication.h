//
//  INInAppApplication.h
//  IndigitallSdk
//
//  Created by indigitall on 14/2/24.
//

#import <Foundation/Foundation.h>
#import "INCoreApplication.h"
#import "INInApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppApplication : INCoreApplication

@property (nonatomic) NSArray<INInApp *> *clearCacheList;
@property (nonatomic) bool clearCacheAll;
@property (nonatomic) bool inAppEnabled;
@property (nonatomic) int inAppServiceSyncTime;
@property (nonatomic) int audienceDeviceTtl;

- (id)init: (NSDictionary *) json fromCache:(bool)isFromCache;
- (void) updateCache;

@end

NS_ASSUME_NONNULL_END
