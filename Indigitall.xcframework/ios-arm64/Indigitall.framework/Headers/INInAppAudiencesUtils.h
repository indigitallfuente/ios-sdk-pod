//
//  INInAppAudiencesUtils.h
//  IndigitallSdk
//
//  Created by indigitall on 15/11/24.
//

#import <Foundation/Foundation.h>
#import "INInAppAudience.h"
NS_ASSUME_NONNULL_BEGIN

@interface INInAppAudiencesUtils : NSObject

+ (BOOL) isAudienceCacheFinish;
+ (void) saveAudienceCache: (int) cacheTtl;
+ (void) saveAudicences: (INInAppAudience *)audience;
+ (INInAppAudience * _Nullable) loadAudicences;


@end

NS_ASSUME_NONNULL_END
