//
//  InboxConstants.h
//  Indigitall
//
//  Created by indigitall on 07/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INAPIConstants.h"
NS_ASSUME_NONNULL_BEGIN

@interface INInboxConstants : INAPIConstants
@property (nonatomic, class, readonly) NSString *domain;
@property (nonatomic, class, readonly) NSString *INBOX_AUTH_TOKEN_EXPIRE;
@property (nonatomic, class, readonly) NSString *INBOX_NOT_REQUESTED;

@end

NS_ASSUME_NONNULL_END
