//
//  InboxAuthenticationCallback.h
//  Indigitall
//
//  Created by indigitall on 07/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "INBaseCallback.h"
NS_ASSUME_NONNULL_BEGIN

@interface INInboxAuthenticationCallback : INBaseCallback


@property (nonatomic) NSString *authToken;

typedef void(^onErrorInbox)(INError * error);
@property (readwrite, copy) onErrorInbox onError;

typedef void(^onSuccessInboxAuth)(NSString * authToken);
@property (readwrite, copy) onSuccessInboxAuth onSuccess;

- (id) initWithOnSuccess: (void(^)(NSString *))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;

@end

NS_ASSUME_NONNULL_END
