//
//  INBackgroundRequestsState.h
//  IndigitallSdk
//
//  Created by indigitall on 30/1/25.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

typedef enum {
    NO_ASSIGNED = 0,
    ENABLED = 1,
    DISABLED = 2,
} INBackgroundRequestsState;

extern NSString* _Nonnull const FormatBackgroundRequestsStateType_toString[];


NS_ASSUME_NONNULL_END
