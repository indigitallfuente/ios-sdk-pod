//
//  INDefaults.h
//  indigitall+objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "INCoreExternalApp.h"
#import "INBackgroundRequestsState.h"

NS_ASSUME_NONNULL_BEGIN

@interface INDefaults : NSObject

@property (nonatomic)NSString *testKey;
@property (nonatomic)NSString *appKey;
@property (nonatomic)NSString *deviceKey;
@property (nonatomic)NSString *productNameKey;
@property (nonatomic)NSString *productVersionKey;
@property (nonatomic)NSString *locationPermissionAskedKey;
@property (nonatomic)NSString *locationPermissionModeKey;
@property (nonatomic)NSString *locationPermissionStatusSentKey;
@property (nonatomic)NSString *visitEventTimestampKey;
@property (nonatomic)NSString *enabledKey;
@property (nonatomic)NSString *configEnabledKey;
@property (nonatomic)NSString *configServiceSyncTimeKey;
@property (nonatomic)NSString *configLastCallTimestampKey;
@property (nonatomic)NSString *configMaintenanceTimeStartKey;
@property (nonatomic)NSString *configMaintenanceTimeEndKey;
@property (nonatomic)NSString *configLocationEnabledKey;
@property (nonatomic)NSString *domain;
@property (nonatomic)NSString *domainInbox;
@property (nonatomic)NSString *externalId;
@property (nonatomic)NSString *inAppEnabled;
@property (nonatomic)NSString *secureSendingEnabledKey;
@property (nonatomic)NSString *secureSendingAppPublicKeyKey;
@property (nonatomic)NSString *sdkVersionKey;
@property (nonatomic)NSString *externalAppsKey;
@property (nonatomic)NSString *externalAppsForPostMethodKey;
@property (nonatomic)NSString *latitudeKey;
@property (nonatomic)NSString *longitudeKey;
@property (nonatomic)NSString *debugLogKey;
@property (nonatomic)NSString *authModeKey;
@property (nonatomic)NSString *avoidCypherKey;
@property (nonatomic)NSString *configV2ServiceSyncTimeKey;

@property (nonatomic)NSString *configV2ContentAvailableKey;
@property (nonatomic)NSString *configV2LocationAccuracyKey;
@property (nonatomic)NSString *configV2LocationDistanceKey;
@property (nonatomic)NSString *configV2LocationEnabledKey;
@property (nonatomic)NSString *configV2LocationUpdateMinutesKey;
@property (nonatomic)NSString *configV2MaintanceWindowStartKey;
@property (nonatomic)NSString *configV2MaintanceWindowEndKey;
@property (nonatomic)NSString *configV2NetworkEventsEnabledKey;
@property (nonatomic)NSString *configV2NetworkUpdateMinutesKey;
@property (nonatomic)NSString *configV2Key;

@property (nonatomic)NSString *observerDisabledKey;
@property (nonatomic)NSString *backgroundRequestsDisabledKey;
@property (nonatomic)NSString *configRemoteBackgroundRequestsDisabledKey;
@property (nonatomic)NSString *observerStateKey;

+ (void) setTest:(nullable NSString *) test;
+ (nullable NSString *) getTest;


//- (id) init;
+ (void) setDeviceId:(NSString *) deviceId;
+ (NSString *) getDeviceId;

+ (void) setAppKey:(NSString *) appKey;
+ (NSString *) getAppKey;

+ (void) setProductName:(NSString *) productName;
+ (NSString *) getProductName;
+ (void) setProductVersion:(NSString *) productVersion;
+ (NSString *) getProductVersion;

+ (BOOL) getLocationPermissionAsked;
+ (void) setLocationPermissionAsked:(BOOL) locationPermissionAsked;
+ (int) getLocationPermissionStatus;
+ (void) setLocationPermissionStatus:(int ) locationPermissionStatus;
+ (int) getLocationPermissionMode;
+ (void) setLocationPermissionMode:(int ) locationPermissionMode;

+ (int64_t) getEventVisitTimestamp;
+ (void) setEventVisitTimestamp:(int64_t) eventVisitTimestamp;

+ (void) setEnabled:(BOOL) enabled;
+ (BOOL) getEnabled;

+ (void) setConfigEnabled:(BOOL) enabled;
+ (BOOL) getConfigEnabled;

+ (void) setConfigServiceSyncTime:(int) config;
+ (int) getConfigServiceSyncTime;

+ (void) setConfigLastCallTimestamp:(int64_t) config;
+ (int64_t) getConfigLastCallTimestamp;

+ (void) setConfigMaintenanceTimeStart:(NSString *) config;
+ (NSString *) getConfigMaintenanceTimeStart;

+ (void) setConfigMaintenanceTimeEnd:(NSString *) config;
+ (NSString *) getConfigMaintenanceTimeEnd;

+ (void) setConfigLocationEnabled:(BOOL) config;
+ (BOOL) getConfigLocationEnabled;

+ (void) setDomain:(NSString *)domain;
+ (NSString *) getDomain;

+ (void) setDomainInbox:(NSString *)domainInbox ;
+ (NSString *) getDomainInbox;

+ (void) setExternalId:(NSString * _Nullable)externalId;
+ (NSString *) getExternalId;

+ (void) setInAppEnabled :(BOOL)inAppEnabled ;
+ (BOOL) getInAppEnabled;

+ (void) setSecureSendingEnabled:(BOOL)secureSendingEnabled;
+ (BOOL) getSecureSendingEnabled;

+ (void) setSecureSendingAppPublicKey: (NSString *)secureSendingAppPublicKey;
+ (NSString *) getSecureSendingAppPublicKey;

+ (void) setSDKVersion:(NSString *)sdkVersion;
+ (NSString *) getSDKVersion;


+ (void) setExternalApps:(NSArray<INCoreExternalApp *> * _Nullable )externalApps ;
+ (NSArray<INCoreExternalApp *> *) getExternalApps;

+ (void) setExternalAppsForPostMethod:(NSArray<INCoreExternalApp *> * _Nullable )externalApps ;
+ (NSArray<INCoreExternalApp *> *) getExternalAppsForPostMethod;


+ (void) setLatitude:(double)latitude;
+ (double) getLatitude;

+ (void) setLongitude:(double)longitude;
+ (double) getLongitude;

+ (void) setLogDebug:(int)debugLog;
+ (int) getLogDebug;

+ (void) setAuthMode:(NSString *)authMode;
+ (NSString *) getAuthMode;

+ (void) setAvoidCypher:(BOOL) avoidCypher;
+ (BOOL) isAvoidCypher;

+ (void) setConfigV2:(NSDictionary *)value;
+ (NSDictionary *) getConfigV2;

+ (void) setConfigV2ServiceSyncTime:(NSString *)serviceSyncTime;
+ (NSString *) getConfigV2ServiceSyncTime;

+ (void) setConfigV2ContentAvailable:(bool)contentAvailable;
+ (bool) getConfigV2ContentAvailable;

+ (void) setConfigV2LocationAccuracy:(int)value;
+ (int) getConfigV2LocationAccuracy;

+ (void) setConfigV2LocationDistance:(int)value;
+ (int) getConfigV2LocationDistance;

+ (void) setConfigV2LocationEnabled:(bool)value;
+ (bool) getConfigV2LocationEnabled;

+ (void) setConfigV2LocationUpdateMinutes:(int)value;
+ (int) getConfigV2LocationUpdateMinutes;

+ (void) setConfigV2MaintanceWindowStart:(NSString *)value;
+ (NSString *) getConfigV2MaintanceWindowStart;

+ (void) setConfigV2MaintanceWindowEnd:(NSString *)value;
+ (NSString *) getConfigV2MaintanceWindowEnd;

+ (void) setConfigV2NetworkEventsEnabled:(bool)value;
+ (bool) getConfigV2NetworkEventsEnabled;

+ (void) setConfigV2NetworkUpdateMinutes:(int)value;
+ (int) getConfigV2NetworkUpdateMinutes;

+ (void) setConfigV2:(NSDictionary *)value;
+ (NSDictionary *) getConfigV2;

+ (void) setBackgroundRequestsDisabled:(BOOL)value;
+ (BOOL) isBackgroundRequestsDisabled ;

+ (void) setConfigRemoteBackgroundRequestsDisabled:(INBackgroundRequestsState)value;
+ (INBackgroundRequestsState) isConfigRemoteBackgroundRequestsDisabled;


@end

NS_ASSUME_NONNULL_END
