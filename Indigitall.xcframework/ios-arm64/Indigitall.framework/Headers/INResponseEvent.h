//
//  ResponseEvent.h
//  indigitall-objc
//
//  Created by indigitall on 04/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface INResponseEvent : INBaseResponse

- (id) initWithCallback: (INBaseCallback *_Nullable) callback;
- (void) processWithData:(NSData *_Nullable)data urlResponse:(NSString *)urlResponse error:(NSError *_Nullable)error;

@end

NS_ASSUME_NONNULL_END
