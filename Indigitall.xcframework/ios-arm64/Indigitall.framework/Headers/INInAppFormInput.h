//
//  INInAppFormInput.h
//  IndigitallInApp
//
//  Created by indigitall on 22/12/22.
//

#import <Foundation/Foundation.h>
#import "INInAppFormType.h"
NS_ASSUME_NONNULL_BEGIN

@interface INInAppFormInput : NSObject

@property (nonatomic) NSString *inputId;
@property (nonatomic) NSString *name;
@property (nonatomic) INInAppFormType type;
@property (nonatomic) BOOL required;
@property (nonatomic) BOOL sendToCustomer;

- (id) initWithDictionary: (NSDictionary *)json;

@end

NS_ASSUME_NONNULL_END
