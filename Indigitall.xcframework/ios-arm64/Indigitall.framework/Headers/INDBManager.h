//
//  DBManager.h
//  Indigitall
//
//  Created by indigitall on 9/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "INInApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface INDBManager : NSObject {
    NSString *databasePath;
}

+ (INDBManager*) getSharedInstance;
- (BOOL) createDB;
- (NSDictionary *) searchData: (char *)query;
- (BOOL) saveData: (char *)query ;
- (BOOL) deleteData: (char *)query;
- (BOOL) updateData: (char *)query;

@end

NS_ASSUME_NONNULL_END
