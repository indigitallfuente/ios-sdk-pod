//
//  INInAppTopic.h
//  inapp
//
//  Created by indigitall on 29/6/22.
//

#import <Foundation/Foundation.h>
#import "INCoreTopic.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppTopic : INCoreTopic
- (id) initWithJson: (NSDictionary *)json;
- (NSDictionary *) toJson;
+ (NSDictionary *) toJson: (INInAppTopic *)topic;
+ (NSArray<NSDictionary *> *) toJsonFromArray: (NSArray< INInAppTopic *>*) arrayTopics;
@end

NS_ASSUME_NONNULL_END
