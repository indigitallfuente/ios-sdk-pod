//
//  INInApFiltersTopic.h
//  IndigitallSdk
//
//  Created by indigitall on 19/7/23.
//

#import <Foundation/Foundation.h>
#import "INInAppTopic.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppFiltersTopic: NSObject

@property (nonatomic) NSArray<INInAppTopic*> *topicsSubscribed;
@property (nonatomic) NSArray<INInAppTopic*> *topicsUnsubscribed;

- (id) initWithJson: (NSDictionary *)json;
- (NSDictionary *) toJson;

@end

NS_ASSUME_NONNULL_END
