//
//  INInboxAction.h
//  IndigitallInbox
//
//  Created by indigitall on 8/3/23.
//

#import <Foundation/Foundation.h>
#import "INCoreAction.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxAction : INCoreAction

- (id) init: (NSDictionary *)json;

@end

NS_ASSUME_NONNULL_END
