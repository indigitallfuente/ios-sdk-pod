//
//  EventRequestInApp.h
//  Indigitall
//
//  Created by indigitall on 6/9/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseRequest.h"
#import "INInAppAction.h"
#import "INInApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface INEventRequestInApp : INBaseRequest

@property (nonatomic) NSString * externalId;
@property (nonatomic) int inAppId;
@property (nonatomic) INInAppAction *inAppAction;
@property (nonatomic) int lastVersionId;
@property (nonatomic) INInApp *inApp;

- (id) initWithInApp: (INInApp *)inApp;
- (id)initWithInAppId: (int)inAppId lastVersionId:(int)lastVersion action: (INInAppAction *)inAppAction;
//- (id) eventPostInAppRequest;
- (id) eventPrint;
- (id) eventClick;
@end

NS_ASSUME_NONNULL_END
