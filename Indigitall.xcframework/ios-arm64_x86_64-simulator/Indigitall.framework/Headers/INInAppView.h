//
//  INInAppView.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import "INInAppError.h"
#import "INInAppHandler.h"
#import "INInAppCallback.h"
#import "INEventRequestInApp.h"
#import "INInAppUtils.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppView : UIView<UIGestureRecognizerDelegate, WKScriptMessageHandler>

@property (strong, nonatomic) void (^didTouch)(INInApp *inApp,UIView *webView, INInAppAction *action, NSString *noAction);
@property (strong, nonatomic) void (^failed)(NSArray<INError *> *error);
@property (strong, nonatomic) void (^formSubmit)(INInApp *inApp);

@property (strong, nonatomic) void (^onShowTimeFinished)(INInApp *inApp,UIView *webView, int showTime);
@property (strong, nonatomic) INInApp *inApp;

@property (nonatomic) WKUserContentController *contentController;

@property (nonatomic) WKWebView *webView;
@property (nonatomic) UIView *view;
@property (nonatomic) INInAppView *current;
@property (strong, nonatomic) NSString *noAction;
- (void)load:(UIView *)view content:(NSString *)content inApp:(INInApp *)inApp didTouch:(void(^)(INInApp *inApp, UIView *webView, INInAppAction *inAppAction, NSString *noAction))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished success:(void(^)(void))success;
- (void)load:(UIView *)view content:(NSString *)content inApp:(INInApp *)inApp failed:(nullable void(^)(NSArray<INError *> *error))failed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit didTouch:(void(^)(INInApp *inApp, UIView *webView, INInAppAction *inAppAction, NSString *noAction))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished success:(void(^)(void))success;
- (id)init;

@end

NS_ASSUME_NONNULL_END
