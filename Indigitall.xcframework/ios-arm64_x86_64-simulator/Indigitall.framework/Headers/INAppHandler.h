//
//  AppHandler.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface INAppHandler : NSObject<UIApplicationDelegate>

+ (void) applicationDidBecomeActive;
+ (void) didFinishLaunchingWithOptions:(NSNotification *)notification;
@end

NS_ASSUME_NONNULL_END
