//
//  InAppTopicCallback.h
//  inapp
//
//  Created by indigitall on 29/6/22.
//

#import <Foundation/Foundation.h>
#import "INBaseCallback.h"
#import "INInAppTopic.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppTopicCallback : INBaseCallback
@property (nonatomic) NSMutableArray<INInAppTopic*> *topic;

//typedef void(^topicCallback)(INTopic * _Nullable success, INError * _Nullable error);
//@property (readwrite, copy) topicCallback topicCallback;

typedef void(^onErrorInAppTopic)(INError * error);
@property (readwrite, copy) onErrorInAppTopic onError;

typedef void(^onSuccessInAppTopic)(NSArray<INInAppTopic *>*topics);
@property (readwrite, copy) onSuccessInAppTopic onSuccess;

- (id)initOnSuccess: (nullable void(^)(NSArray<INInAppTopic *>*topics))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;
@end

NS_ASSUME_NONNULL_END
