//
//  HmacUtils.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>
#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>
#import "INDefaults.h"

NS_ASSUME_NONNULL_BEGIN

@interface INHmacUtils : NSObject
- (NSString *)hmacsha256:(NSString *)data secret:(NSString *)key;
@end

NS_ASSUME_NONNULL_END
