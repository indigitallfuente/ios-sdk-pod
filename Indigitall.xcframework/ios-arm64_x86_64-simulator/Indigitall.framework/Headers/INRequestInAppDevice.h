//
//  INRequestInAppDevice.h
//  IndigitallSdk
//
//  Created by indigitall on 30/11/23.
//

#import <Foundation/Foundation.h>
#import "INInAppDevice.h"
#import "INBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface INRequestInAppDevice : INBaseRequest

@property (nonatomic) INInAppDevice *device;

- (id) init;
- (id) postDeviceRequest;
@end

NS_ASSUME_NONNULL_END
