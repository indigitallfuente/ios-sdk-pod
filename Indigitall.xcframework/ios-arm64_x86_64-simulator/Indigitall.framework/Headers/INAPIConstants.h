//
//  APIConstants.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INAPIConstants : NSObject

//@property (nonatomic, class, readonly) NSString * domain;
@property (nonatomic, class, readonly) NSString * pathApplication;
@property (nonatomic, class, readonly) NSString * pathApplicationAll;
@property (nonatomic, class, readonly) NSString * pathDevice;
@property (nonatomic, class, readonly) NSString * pathTopic;
@property (nonatomic, class, readonly) NSString * pathEvent;
@property (nonatomic, class, readonly) NSString * pathEventPush;
@property (nonatomic, class, readonly) NSString * pathEventVisit;
@property (nonatomic, class, readonly) NSString * pathEventPermission;
@property (nonatomic, class, readonly) NSString * pathEventLocation;

@property (nonatomic, class, readonly) NSString * params_pushToken;
@property (nonatomic, class, readonly) NSString * params_externalApps;

@property (nonatomic, class, readonly) NSString * clickedButton;
@property (nonatomic, class, readonly) NSString * action;
@property (nonatomic, class, readonly) NSString * type;
@property (nonatomic, class, readonly) NSString * typeApp;
@property (nonatomic, class, readonly) NSString * typeURL;
@property (nonatomic, class, readonly) NSString * typeWebview;
@property (nonatomic, class, readonly) NSString * typeCall;
@property (nonatomic, class, readonly) NSString * typeMarket;
@property (nonatomic, class, readonly) NSString * typeShare;
@property (nonatomic, class, readonly) NSString * typeWallet;
@property (nonatomic, class, readonly) NSString * destroy;
@property (nonatomic, class, readonly) NSString * topics;

@property (nonatomic, class, readonly) NSString * application_externalApps;
@property (nonatomic, class, readonly) NSString * application_externalApps_id;
@property (nonatomic, class, readonly) NSString * application_externalApps_name;
@property (nonatomic, class, readonly) NSString * application_externalApps_iosCode;
@property (nonatomic, class, readonly) NSString * application_configuration;
@property (nonatomic, class, readonly) NSString * application_configuration_enabled;
@property (nonatomic, class, readonly) NSString * application_configuration_serviceSyncTime;
@property (nonatomic, class, readonly) NSString * application_configuration_maintenanceWindow;
@property (nonatomic, class, readonly) NSString * application_configuration_maintenanceWindow_end;
@property (nonatomic, class, readonly) NSString * application_configuration_maintenanceWindow_start;
@property (nonatomic, class, readonly) NSString * application_configuration_locationEnabled;
@property (nonatomic, class, readonly) NSString * application_configuration_networkEventsEnabled;
@property (nonatomic, class, readonly) NSString * application_configuration_networkUpdateMinutes;
@property (nonatomic, class, readonly) NSString * application_configuration_secureSendingEnabled;
@property (nonatomic, class, readonly) NSString * application_configuration_secureSendingAppPublicKey;
@property (nonatomic, class, readonly) NSString * application_configuration_background_requests_disabled;


@property (nonatomic, class, readonly) NSString * device_enabled;
@property (nonatomic, class, readonly) NSString * topics_topics;
@property (nonatomic, class, readonly) NSString * topics_name;
@property (nonatomic, class, readonly) NSString * topics_code;
@property (nonatomic, class, readonly) NSString * topics_parentCode;
@property (nonatomic, class, readonly) NSString * topics_visible;
@property (nonatomic, class, readonly) NSString * topics_subscribed;
@property (nonatomic, class, readonly) NSString * application_configuration_inAppEnabled;
@property (nonatomic, class, readonly) NSString * application_configuration_inboxAuthMode;

@property (nonatomic, class, readonly) NSString * buttons;
@property (nonatomic, class, readonly) NSString * buttonLabel;

@end

NS_ASSUME_NONNULL_END
