//
//  InAppAPIClient.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInAppLog.h"
#import "INBaseClient.h"
#import "INRequestInApp.h"
#import "INEventRequestInApp.h"
#import "INInAppCustomerRequest.h"
#import "INInAppCallback.h"
#import "INFormRequestInApp.h"
#import "INRequestInAppDevice.h"
#import "INResponseInAppDevice.h"
#import "INInAppApplicationCallback.h"
#import "INBaseApplicationRequest.h"
#import "INInAppAudienceRequest.h"
#import "INInAppAudienceCallback.h"
#import "INInAppAudienceResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppAPIClient : INBaseClient
+ (void) getInAppCampaign:(INRequestInApp *)request completion:(INInAppCallback *)callback;
+ (void) loadInApp: (INRequestInApp *)request completion:(INInAppCallback *)callback;

//+ (void) postEventRequest: (EventRequestInApp *)request completion:(BaseCallback *)callback;
+ (void) getTopics: (INBaseRequest *)request completion:(INBaseCallback *)callback;
+ (void) postEventClickRequest: (INEventRequestInApp *)request completion:(INBaseCallback *)callback;
+ (void) postEventPrintRequest: (INEventRequestInApp *)request completion:(INBaseCallback *)callback;

+ (void) postEventForm: (INFormRequestInApp *)request completion:(INBaseCallback *)callback;

+ (void) putCustomerField: (INInAppCustomerRequest *)request;

+ (void) postDevice: (INRequestInAppDevice *)request ;

+ (void) getApplicationWithInAppChannel: (INBaseApplicationRequest *) request
                              onApplicationSuccess: (nullable void(^)(INInAppApplication *inAppApplication))onSuccess
                                onApplicationError:(nullable void(^)(INError *error))onError;

+ (void) getAudiences: (INInAppAudienceRequest *)request
        onAudienceSuccess: (nullable void(^)(INInAppAudience *audience))onSuccess
        onAudienceError:(nullable void(^)(INError *error))onError;
@end

NS_ASSUME_NONNULL_END
