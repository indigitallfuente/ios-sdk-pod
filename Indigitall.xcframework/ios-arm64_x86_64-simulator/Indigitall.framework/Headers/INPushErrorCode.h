//
//  INPushErrorCode.h
//  Indigitall
//
//  Created by indigitall on 23/5/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
 
NS_ASSUME_NONNULL_BEGIN

typedef enum {
    PUSH_INITIALIZATION_ERROR = 1000,
    PUSH_RECEPTION_ERROR = 1200,
    PUSH_TOPICS_ERROR = 1400,
    PUSH_DEVICE_ERROR = 1500,
    PUSH_EVENT_ERROR = 1600,
    PUSH_TOKEN_ERROR = 1700,
    PUSH_FCM_REGISTRATION = 1701,
    PUSH_FCM_GET_INSTANCE = 1702,
    PUSH_BAD_REQUEST = 1704
} INPushErrorCode;

extern NSString* _Nonnull const FormatStatePushError_toString[];

NS_ASSUME_NONNULL_END
