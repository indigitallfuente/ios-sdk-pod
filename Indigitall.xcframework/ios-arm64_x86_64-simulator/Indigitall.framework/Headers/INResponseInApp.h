//
//  ResponseInApp.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseResponse.h"
#import "INInApp.h"
#import "INInAppTopic.h"

NS_ASSUME_NONNULL_BEGIN

@interface INResponseInApp : INBaseResponse

@property (nonatomic) INInApp *inApp;
@property (nonatomic) NSArray<INInAppTopic*> *topics;

- (id)initWithCallback:(INBaseCallback *_Nullable)callback;

- (void) processWithData:(NSData *_Nullable)data urlResponse:(NSURL *)urlResponse error:(NSError *_Nullable)error;

@end

NS_ASSUME_NONNULL_END
