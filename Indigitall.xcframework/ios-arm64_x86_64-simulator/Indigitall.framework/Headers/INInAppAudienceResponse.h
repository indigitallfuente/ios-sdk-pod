//
//  INInAppAudienceResponse.h
//  IndigitallSdk
//
//  Created by indigitall on 18/11/24.
//

#import <Foundation/Foundation.h>
#import "INBaseResponse.h"
#import "INInAppAudience.h"
NS_ASSUME_NONNULL_BEGIN

@interface INInAppAudienceResponse : INBaseResponse

@property (nonatomic) INInAppAudience *audience;

- (id) initWithCallback: (INBaseCallback *_Nullable) callback;
- (void) processWithData:(NSData *_Nullable)data urlResponse:(NSURL *)urlResponse error:(NSError *)error;

@end

NS_ASSUME_NONNULL_END
