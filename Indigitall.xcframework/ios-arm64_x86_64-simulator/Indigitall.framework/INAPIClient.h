//
//  APIClient.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseClient.h"
//#import "BaseCallback.h"
#import "INRequestDevice.h"
#import "INRequestApp.h"
#import "INRequestEvent.h"
#import "INApplicationCallback.h"
#import "INTopicCallback.h"
#import "INDeviceCallback.h"

NS_ASSUME_NONNULL_BEGIN

@interface INAPIClient :INBaseClient

+ (void) loadConfiguration: (INRequestApp *)request completion:(INApplicationCallback *)callback;
+ (void) postDevice: (INRequestDevice *)request completion:(INDeviceCallback *)callback;
+ (void) putDevice: (int)status request:(INRequestDevice *)request completion:(INDeviceCallback *)callback onDeviceNotUpdated: (void(^)(INDevice * device))onDeviceNotUpdated;
+ (void) getDevice: (INRequestDevice *)request completion:(INDeviceCallback *)callback;
+ (void) getTopics: (INRequestDevice *)request completion:(INTopicCallback *)callback;
+ (void) subscribeTopics: (INRequestDevice *)request completion:(INTopicCallback *)callback;
+ (void) unsubscribeTopics: (INRequestDevice *)request completion:(INTopicCallback *)callback;
+ (void) eventPush: (INRequestEvent *)request completion:(INBaseCallback *)callback;
+ (void) eventVisit: (INRequestEvent *)request;
+ (void) eventPermission: (INRequestEvent *)request completion:(INBaseCallback *)callback;
+ (void) eventLocation: (INRequestEvent *)request completion:(INBaseCallback *)callback;
+ (void) eventCustomSend: (INRequestEvent *)request completion:(INBaseCallback *)callback;
+ (void) postSendJourneyEvent: (INRequestEvent *) request completion: (INBaseCallback *) callback;
+ (void) eventNetwork: (INRequestEvent *)request completion:(INBaseCallback *)callback;
+ (void) eventPushReceived: (INRequestEvent *)request;

@end

NS_ASSUME_NONNULL_END
