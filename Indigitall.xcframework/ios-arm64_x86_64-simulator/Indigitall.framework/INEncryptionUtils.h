//
//  NSData+Encryption.h
//  Indigitall
//
//  Created by indigitall on 11/06/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface INEncryptionUtils: NSObject//NSData (Encryption)

+ (NSData *)AES256EncryptWithKey:(NSString *)key withData: (NSData *)data;
+ (NSData *)AES256DecryptWithKey:(NSString *)key withData: (NSData *)data;

@end

