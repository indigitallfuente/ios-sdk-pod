//
//  INInAppFilters.h
//  IndigitallSdk
//
//  Created by indigitall on 19/7/23.
//

#import <Foundation/Foundation.h>
#import "INCoreExternalApp.h"
#import "INInAppFiltersTopic.h"
NS_ASSUME_NONNULL_BEGIN

@interface INInAppFilters : NSObject
@property (nonatomic) NSArray* platforms;
@property (nonatomic) NSArray<NSNumber*> *areas;
@property (nonatomic) NSDictionary *audience;
@property (nonatomic) INInAppFiltersTopic *topics;
@property (nonatomic) NSArray<INCoreExternalApp*> *externalApps;
@property (nonatomic) NSArray *deviceCodes;
@property (nonatomic) BOOL deviceCodesActive;
@property (nonatomic) NSArray *deviceTypes;
@property (nonatomic) NSArray *browserTypes;
@property (nonatomic) BOOL activePushDevices;

- (id) initWithJson: (NSDictionary *)json;
- (NSDictionary *) toJson;


@end



NS_ASSUME_NONNULL_END
