//
//  CustomerLinkCallback.h
//  Indigitall
//
//  Created by indigitall on 15/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseCallback.h"

NS_ASSUME_NONNULL_BEGIN

@interface INCustomerLinkCallback : INBaseCallback

typedef void(^onErrorCustomerLink)(INError * error);
@property (readwrite, copy) onErrorCustomerLink onError;

typedef void(^onSuccessCustomerLink)(void);
@property (readwrite, copy) onSuccessCustomerLink onSuccess;

- (id)initWithOnSuccess: (void(^)(void))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;


@end

NS_ASSUME_NONNULL_END
