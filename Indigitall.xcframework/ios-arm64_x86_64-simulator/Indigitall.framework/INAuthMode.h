//
//  INAuthMode.h
//  IndigitallCore
//
//  Created by indigitall on 8/3/23.
//
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum{
    NONE,
    DEFAULT,
    WEBHOOK
}INAuthMode ;

extern NSString* _Nonnull const FormatAuthMode_toString[];

NS_ASSUME_NONNULL_END
