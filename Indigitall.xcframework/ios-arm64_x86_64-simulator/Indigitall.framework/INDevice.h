//
//  INDevice.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INCoreDevice.h"
NS_ASSUME_NONNULL_BEGIN
@class DeviceHandler;
@interface INDevice : INCoreDevice

@property (nonatomic, readonly) NSString * JSON_DEVICE_ID;
@property (nonatomic, readonly) NSString * JSON_PUSH_TOKEN;

/// Indigital deviceID
@property (nonatomic, nullable) NSString *deviceID;

//@property (nonatomic) INCoreExternalApp *externalApplications;

// MARK: - Local properties
/// Push notification token
@property (nonatomic) NSString *pushToken;

/// SKD versión


- (id)init;
- (id)initWithNoData;
- (id)initWithData: (NSMutableDictionary * _Nullable) data;

- (void) update: (NSDictionary *)json;
- (NSMutableDictionary *)toJsonForPutMethod: (BOOL)isPutMethod;

@end

NS_ASSUME_NONNULL_END
