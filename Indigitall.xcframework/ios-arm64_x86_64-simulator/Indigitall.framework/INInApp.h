//
//  INInApp.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInAppProperties.h"
#import "INInAppFilters.h"
#import "INInAppShow.h"
NS_ASSUME_NONNULL_BEGIN

@interface INInApp : NSObject

@property (nonatomic) int inAppId;
@property (nonatomic) int inAppWidth;
@property (nonatomic) int inAppHeight;
@property (nonatomic) int lastVersionId;
@property (nonatomic) NSString *code;
@property (nonatomic) INInAppProperties *properties;
@property (nonatomic) int showOnce;
@property (nonatomic) NSString * expiredDate;
@property (nonatomic) NSString * creationDate;
@property (nonatomic) double cacheTtl;
@property (nonatomic) INInAppShow * inAppShow;
@property (nonatomic) NSDictionary *customData;
@property (nonatomic) int version;
@property (nonatomic) int inAppVersion;
@property (nonatomic) INInAppFilters *filters;
@property (nonatomic) NSString *name;

- (id) init;

- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *)toJson: (INInApp *)inApp;

@end


NS_ASSUME_NONNULL_END
