//
//  INPush.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "INPushAction.h"
#import "INPushButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface INPush : NSObject
// MARK: - Properties
/// The push indigitall ID
@property (nonatomic) long pushId;
/// Public app identifier from indigitall
@property (nonatomic) NSString *appKey;
/// A string containing the title of the push
@property (nonatomic) NSString *title;
/// A string containing the body of the push
@property (nonatomic) NSString *body;
/// A string containing the url of the push image
@property (nonatomic) NSString *image;
/// A string containing the url of the push gif
@property (nonatomic) NSString *gif;
/// Astring containing the extra info appended in the indigitall panel
@property (nonatomic) NSString *data;
/// An action that will execute when the user interacts with the push
@property (nonatomic) INPushAction *action;
/// An array of push available buttons
@property (nonatomic) NSArray<INPushButton *> *buttons;
/// Secured data encrypted
@property (nonatomic) NSString *securedData;
/// SendingId
@property (nonatomic) NSString *sendingId;
/// Campign Id
@property (nonatomic) NSString *campaignId;
/// Content state json for live action push 
@property (nonatomic) NSDictionary *contentState;
/// notification or button clicked 
@property (nonatomic) int clickedButton;
/// journeyStateId
@property (nonatomic) int journeyStateId;
///// journeyExecution
//@property (nonatomic) int journeyExecution;
///// cjCurrentStateId
//@property (nonatomic) int cjCurrentStateId;
/// send event ack
@property (nonatomic) BOOL sendEventAck;
/// send applicationID
@property (nonatomic) int applicationId;

@property (nonatomic) NSDictionary *jsonPush;
- (id)init: (NSDictionary *)json;
- (NSDictionary *) prepareStatisticsWithDictionary: (NSDictionary* )requestValues;

@end

NS_ASSUME_NONNULL_END
