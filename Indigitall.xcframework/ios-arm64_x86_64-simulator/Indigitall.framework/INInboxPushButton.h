//
//  INInboxPushButton.h
//  IndigitallInbox
//
//  Created by indigitall on 8/3/23.
//

#import <Foundation/Foundation.h>
#import "INCorePushButton.h"
#import "INInboxAction.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxPushButton : INCorePushButton

@property INInboxAction *action;

-(id)init:(NSDictionary *)json;
@end

NS_ASSUME_NONNULL_END
