//
//  INInAppFormType.h
//  IndigitallInApp
//
//  Created by indigitall on 22/12/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    TEXT,
    NUMBER,
    RADIO,
    CHECKBOX,
    SELECT
} INInAppFormType;

extern NSString* _Nonnull const FormatInAppFormType_toString[];


NS_ASSUME_NONNULL_END
