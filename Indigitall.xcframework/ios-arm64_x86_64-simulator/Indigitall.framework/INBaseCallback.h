//
//  BaseCallback.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import "INError.h"
NS_ASSUME_NONNULL_BEGIN

@interface INBaseCallback : NSObject

@property (nonatomic) INError *error;

- (id)init;
- (void) proccessDataWithStatusCode: (long) statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable) data;

@end
NS_ASSUME_NONNULL_END
