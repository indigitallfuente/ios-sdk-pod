//
//  RequestApp.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface INRequestApp : INBaseRequest
- (id) init;
@end

NS_ASSUME_NONNULL_END
