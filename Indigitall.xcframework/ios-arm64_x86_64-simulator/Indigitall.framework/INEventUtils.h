//
//  EventUtils.h
//  IndigitallInApp
//
//  Created by indigitall on 7/10/22.
//

#import <Foundation/Foundation.h>
#import "INInApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface INEventUtils : NSObject
+ (NSString *) generateUUID;
+ (void) eventPrintInAppRequest: (INInApp *)inApp;
+ (void) eventClickInAppRequest: (INInApp *)inApp;
+ (void) sendEventFormToCustomer: (NSDictionary *)formEvent;
+ (void) sendEventForm: (INInApp *)inApp jsonForm:(NSDictionary *)formEvent;
@end

NS_ASSUME_NONNULL_END
