//
//  INInAppApplicationCallback.h
//  IndigitallSdk
//
//  Created by indigitall on 14/2/24.
//

#import <Foundation/Foundation.h>
#import "INBaseCallback.h"
#import "INInAppApplication.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppApplicationCallback : INBaseCallback
@property (nonatomic) INInAppApplication *application;

typedef void(^onError)(INError * error);
@property (readwrite, copy) onError onError;

typedef void(^InAppApplicationonSuccess)(INInAppApplication * application);
@property (readwrite, copy) InAppApplicationonSuccess onSuccess;

- (id)initWithOnSuccess: (void(^)(INInAppApplication *))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;

@end

NS_ASSUME_NONNULL_END
