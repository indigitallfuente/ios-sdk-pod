//
//  INPermissionType.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
//
//@interface INPermissionType : NSObject
//@property (nonatomic) NSString *push;
//@property (nonatomic) NSString *location;

//@end

typedef enum {
    push,
    location
} INPermissionType;

extern NSString* _Nonnull const FormatType_toString[];
NS_ASSUME_NONNULL_END
