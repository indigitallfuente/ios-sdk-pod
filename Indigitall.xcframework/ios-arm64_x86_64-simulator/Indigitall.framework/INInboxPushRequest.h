//
//  InboxPushRequest.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "INInboxDefaults.h"
#import "INInboxStatus.h"
#import "INInboxStatus.h"
#import "INBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxPushRequest : INBaseRequest

@property (nonatomic) NSString *externalId;
@property (nonatomic) NSNumber *page;
@property (nonatomic) NSNumber *pageSize;
@property (nonatomic) NSString *dateFrom;
@property (nonatomic) NSString *dateTo;
@property (nonatomic) NSArray<NSString *> *statusList;
@property (nonatomic) NSString * status;
@property (nonatomic) NSString * initialStatus;
@property (nonatomic) NSArray<NSNumber *> *sendingIdList;
@property (nonatomic) NSNumber *sendingId;

- (id) init;

- (id) getInboxPushRequest;

- (id) putInboxPushRequest;

- (id) getInboxPushRequestWithSendingId;

- (id) putInboxPushRequestWithSendingId;


@end

NS_ASSUME_NONNULL_END
