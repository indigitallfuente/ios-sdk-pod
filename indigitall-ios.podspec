Pod::Spec.new do |s|
  s.name             = 'indigitall-ios'
  s.version          = '6.11.1'
  s.summary          = 'indigital sdk.'

  s.description      = <<-DESC
, indigital inapp
                       DESC

  s.homepage         = 'https://bitbucket.org/indigitallfuente/ios-sdk-pod'
  s.license          = { :type => 'MIT', :file => 'LICENSE.md' }
  s.author           = { 'Smart2me S.L.' => 'sdk@indigitall.com' }
  s.source           = { :git => 'https://bitbucket.org/indigitallfuente/ios-sdk-pod.git', :tag => s.version.to_s }

  s.ios.deployment_target = '12'
  s.ios.vendored_frameworks = 'Indigitall.xcframework'

 
  #s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  #s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

end

