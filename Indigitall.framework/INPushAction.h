//
//  INPushAction.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "INCoreAction.h"
#import "INPushConstants.h"

NS_ASSUME_NONNULL_BEGIN

@interface INPushAction : INCoreAction

- (id) init: (NSDictionary *)json;
@end

NS_ASSUME_NONNULL_END
