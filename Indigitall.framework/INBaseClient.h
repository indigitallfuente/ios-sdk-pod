//
//  BaseClient.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseRequest.h"
#import "INBaseResponse.h"
#import "INLog.h"
#import "INChannel.h"
#import "INBaseApplicationRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface INBaseClient : NSObject

//@property (nonatomic) Result *result;

@property (nonatomic) bool serviceEnabled;

+ (void) get: (NSString *)endpoint request:(INBaseRequest *) request baseResponse: (INBaseResponse *)response;
+ (void) post: (NSString *)endpoint request:(INBaseRequest *) request baseResponse: (INBaseResponse *)response;
+ (void) put: (NSString *)endpoint request:(INBaseRequest *) request baseResponse: (INBaseResponse *)response;
+ (void) delete: (NSString *)endpoint request:(INBaseRequest *) request baseResponse: (INBaseResponse *)response;
+ (void) manage: (NSMutableURLRequest *)request body:(nullable NSDictionary *)bodyJson baseResponse: (INBaseResponse *)response;

+ (void) getApplicationWithRequest: (INBaseApplicationRequest *) request withCallback: (INBaseCallback *) callback;

@end

NS_ASSUME_NONNULL_END
