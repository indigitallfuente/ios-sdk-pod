//
//  InboxCountersRequest.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxCountersRequest : INBaseRequest

@property (nonatomic) NSString *externalId;

- (id) init;
- (id) getInboxCounters;


@end

NS_ASSUME_NONNULL_END
