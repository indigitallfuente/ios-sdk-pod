//
//  ResponseTopics.h
//  Indigitall
//
//  Created by indigitall on 17/10/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import "INBaseResponse.h"
#import "INTopic.h"

NS_ASSUME_NONNULL_BEGIN

@interface INResponseTopics : INBaseResponse

@property (nonatomic) NSMutableArray<INTopic *> *topics;
    
- (id) initWithCallback: (INBaseCallback *_Nullable) callback;
- (void) processWithData:(NSData *_Nullable)data urlResponse:(NSURL *)urlResponse error:(NSError *_Nullable)error;

@end

NS_ASSUME_NONNULL_END
