//
//  ApplicationCallback.h
//  Indigitall
//
//  Created by indigitall on 03/10/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import "INApplication.h"
#import "INBaseCallback.h"

NS_ASSUME_NONNULL_BEGIN

@interface INApplicationCallback : INBaseCallback

@property (nonatomic) INApplication *application;

typedef void(^onError)(INError * error);
@property (readwrite, copy) onError onError;

typedef void(^onSuccess)(INApplication * application);
@property (readwrite, copy) onSuccess onSuccess;

- (id)initWithOnSuccess: (void(^)(INApplication *))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;

@end

NS_ASSUME_NONNULL_END
