//
//  InAppUtils.h
//  Indigitall
//
//  Created by indigitall on 12/3/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "INInApp.h"
#import "INInAppError.h"
#import "INInAppDefaults.h"
#import "INDateUtils.h"
#import "INEventRequestInApp.h"
#import "INInAppAPIClient.h"
#import "INInAppAPIConstants.h"
#import "INInAppTopic.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppUtils : NSObject

+ (void) inAppWasGot: (NSString *)inAppCode
            didFound: (void(^)(INInApp *inApp))didFound
            notFound: (void(^)(void))notFound
             isPopUp: (BOOL) popUp
                view: (nullable UIView *)view
           closeIcon:(nullable UIButton *)closeIcon
   closeIconDisabled:(Boolean)closeIconDisabled
           didAppear:(nullable void(^)(void))didAppear
           didCancel:(nullable void(^)(void))didCancel
  didTouchWithAction:(nullable void(^)(INInAppAction *inAppAction))action
        didDismissed:(nullable void(^)(void))didDismissed
  onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished
          didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired
    didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes
         didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut
   didDismissForever:(nullable void(^)(INInApp *inApp, INError *error))didDismissForever
             success:(nullable void(^)(INInApp *))success
              failed:(nullable void(^)(INError *error))failed
          formFailed:(nullable void(^)(NSArray <INInAppError *>*error))formFailed
          formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

+ (INInAppError *) wasInAppPassedFilterProperties: (INInApp *) inApp;

+ (void) inAppFilterControl: (INInApp *)inApp
                   didFound: (void(^)(INInApp *inApp))didFound
                   notFound: (void(^)(void))notFound
                    isPopUp: (BOOL) popUp
                       view: (UIView *)view
                  inAppCode: (NSString *)inAppCode
                  closeIcon:(nullable UIButton *)closeIcon
          closeIconDisabled:(Boolean)closeIconDisabled
                  didAppear:(nullable void(^)(void))didAppear
                  didCancel:(nullable void(^)(void))didCancel
         didTouchWithAction:(nullable void(^)(INInAppAction *inAppAction))action
               didDismissed:(nullable void(^)(void))didDismissed
         onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished
                 didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired
           didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes
                didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut
          didDismissForever:(nullable void(^)(INInApp *inApp, INError *error))didDismissForever
                    success:(nullable void(^)(INInApp *))success
                     failed:(nullable void(^)(INError *error))failed
                 formFailed:(nullable void(^)(NSArray <INInAppError *>*error))formFailed
                 formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

+ (void) updateInAppGetWithInAppDb: (INInApp *)inAppDb
                           isPopUp: (BOOL) popUp
                              view: (UIView *)view
                         inAppCode: (NSString *)inAppCode
                         closeIcon:(nullable UIButton *)closeIcon
                 closeIconDisabled:(Boolean)closeIconDisabled
                         didAppear:(nullable void(^)(void))didAppear
                         didCancel:(nullable void(^)(void))didCancel
                didTouchWithAction:(nullable void(^)(INInAppAction *inAppAction))action
                      didDismissed:(nullable void(^)(void))didDismissed
                onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished
                        didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired
                  didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes
                       didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut
                 didDismissForever:(nullable void(^)(INInApp *inApp, INError *error))didDismissForever
                           success:(nullable void(^)(INInApp *))success
                            failed:(nullable void(^)(INError *error))failed
                        formFailed:(nullable void(^)(NSArray <INInAppError *>*error))formFailed
                        formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

+ (void) addNewInApp:(INInApp *)inApp;
+ (void) inAppWasTappedWithInApp:(INInApp *)inApp;

+ (void) inAppOpenUrl:  (INInAppAction *) action;

+ (BOOL) isInAppDismissForever: (INInApp *)inApp DEPRECATED_ATTRIBUTE;
+ (void) addNewInAppToDismissForever:(INInApp *)inApp;

+ (NSString *) setExternalCode: (NSString *) code;
+ (INInAppAction *) getInAppActionFromString: (NSString *)actionString;
+ (void) inAppWasShownWithInApp: (INInApp *)inApp
                     didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired
            didShowMoreThanOnce:(nullable void(^)(INInApp *inApp, INError *error))didShowMoreThanOnce
                    didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut
                      onSuccess: (void(^)(void))onSuccess;
@end

NS_ASSUME_NONNULL_END
