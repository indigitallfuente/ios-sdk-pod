//
//  INInAppDefaults.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INDefaults.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppDefaults : INDefaults


@property (nonatomic)NSString *domainInApp;
@property (nonatomic)NSString *inAppShowOnceKey;
@property (nonatomic)NSString *inAppTimesClickedKey;
@property (nonatomic)NSString *dismissForeverKey;
@property (nonatomic)NSString *getInAppWasGotKey;
@property (nonatomic)NSString *topicListSubscribedKey;
@property (nonatomic)NSString *inAppDatabaseVersionKey;
@property (nonatomic)NSString *CLOSE_POPUP_WHEN_CLICKED_KEY;
@property (nonatomic)NSString *CONFIG_INAPP_V2_SERVICE_SYNC_TIME;
@property (nonatomic)NSString *AUDIENCE_DATE_CACHE_TTL;
@property (nonatomic)NSString *INAPP_AUDIENCES;
@property (nonatomic)NSString *INAPP_AUDIENCES_CACHE;
@property (nonatomic)NSString *INAPP_AUDIENCES_LAST_UPDATE;

+ (void) setDomainInApp:(NSString *)domainInApp;
+ (NSString *) getDomainInApp;

+ (void) setInAppShowOnce:(NSString *)showOnce;
+ (NSString *) getInAppShowOnce;

+ (void) setInAppTimesClicked:(NSString *)timesClicked;
+ (NSString *) getInAppTimesClicked;

+ (void) setInAppDismissForever:(NSString *)dismissForever;
+ (NSString *) getInAppDismissForever;

+ (void) setInAppWasGot:(NSString *)inAppWasGot;
+ (NSString *) getInAppWasGot;

+ (void) setInAppTopicListSubscribed:(NSArray *)inAppTopicListSubscribed;
+ (NSArray *) getInAppTopicListSubscribed;

+ (void) setInAppDatabaseVersion:(int)version;
+ (int) getInAppDatabaseVersion;

+ (void) setClosePopupWhenClicked:(BOOL)value;
+ (BOOL) getClosePopupWhenClicked;

+ (void) setConfigInAppV2ServiceSyncTime:(NSString *)serviceSyncTime;
+ (NSString *) getConfigInAppV2ServiceSyncTime;

+ (void) setAudienceModel:(NSString * _Nullable)audience;
+ (NSString * _Nullable) getAudienceModel;

+ (void) setAudienceCache:(int)cache;
+ (int) getAudienceCache;

+ (void) setInAppAudienceDateCacheTtl:(NSString * _Nullable)dateCacheTtl;
+ (NSString * _Nullable) getInAppAudienceDateCacheTtl;

+ (void) setAudienceLastUpdate:(NSString * _Nullable)date;
+ (NSString * _Nullable) getAudienceLastUpdate;



@end

NS_ASSUME_NONNULL_END
