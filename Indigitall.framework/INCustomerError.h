//
//  INCustomerError.h
//  Indigitall
//
//  Created by indigitall on 23/5/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INError.h"
#import "INCustomerErrorCode.h"

NS_ASSUME_NONNULL_BEGIN

@interface INCustomerError : INError

@property (readwrite, nonatomic) INCustomerErrorCode customerErrorCode;
@property (readwrite, nonatomic) NSString *customerErrormessage;

- (id)initWithErrorCode:(INCustomerErrorCode)errorCode descriptionMessage:(nullable NSString *)descriptionMessage;


@end

NS_ASSUME_NONNULL_END
