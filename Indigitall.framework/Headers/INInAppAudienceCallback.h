//
//  INInAppAudienceCallback.h
//  IndigitallSdk
//
//  Created by indigitall on 15/11/24.
//

#import <Foundation/Foundation.h>
#import "INBaseCallback.h"
#import "INInAppAudience.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppAudienceCallback : INBaseCallback

@property (nonatomic) INInAppAudience *audience;

typedef void(^onErrorInAppAudience)(INError *error);
@property (readwrite, copy) onErrorInAppAudience onError;

typedef void(^onSuccesInAppAudience)(INInAppAudience * _Nullable audience);
@property (readwrite, copy) onSuccesInAppAudience onSuccess;

- (id)initWithOnSuccess: (void(^)(INInAppAudience *audience))onSuccess onError:(void(^)(INError * error))onError;

- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;

@end

NS_ASSUME_NONNULL_END
