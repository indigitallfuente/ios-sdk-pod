//
//  InboxAuthenticationResponse.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "INBaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxAuthenticationResponse : INBaseResponse



- (id)initWithCallback:(INBaseCallback *_Nullable)callback;

- (void) processWithData:(NSData *_Nullable)data urlResponse:(NSURL *)urlResponse error:(NSError *_Nullable)error;


@end

NS_ASSUME_NONNULL_END
