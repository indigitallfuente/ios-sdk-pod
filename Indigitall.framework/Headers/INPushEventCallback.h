//
//  PushEventCallback.h
//  IndigitallPush
//
//  Created by indigitall on 8/3/23.
//

#import <Foundation/Foundation.h>
#import "INEventCallback.h"
NS_ASSUME_NONNULL_BEGIN

@interface INPushEventCallback : INEventCallback
- (id)initWithOnSuccess: (void(^)(void))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;
@end

NS_ASSUME_NONNULL_END
