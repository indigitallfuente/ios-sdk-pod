//
//  CoreConfig.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INLogLevel.h"
#import "INCoreApplication.h"
NS_ASSUME_NONNULL_BEGIN

@interface INCoreConfig : NSObject

typedef enum permissionMode {
    manual=0,
    automatic=1,
    provisional=2
} PermissionMode;

@property (nonatomic) INCoreConfig *current;
// MARK: - Properties
@property (nonatomic,readwrite) NSString * domain;
/// Public app identifier from indigitall
@property (nonatomic) NSString * appKey;
/// The location permission mode - manual by default
@property (nonatomic) PermissionMode locationPermissionMode;
/// Enable debug mode in order to see more info in logs - false by default
@property (nonatomic) INLogLevel debugMode;
@property (nonatomic) BOOL avoidCypher;
@property (nonatomic) BOOL backgroundRequestsDisabled;

+ (id) current;
- (BOOL) configEnabled;
- (id) init;
- (id) initWithAppKey:(NSString *) appKey;
- (id) initWithJSON: (NSDictionary *)json;
- (void) setProductVersion:(NSString *) productVersion;
- (void) setProductName: (NSString *) productName;
- (void) enabled:(BOOL)enabled;
- (void) updateConfig;
@end

NS_ASSUME_NONNULL_END
