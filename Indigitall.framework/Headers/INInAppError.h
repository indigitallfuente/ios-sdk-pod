//
//  INInAppError.h
//  Indigitall
//
//  Created by indigitall on 23/5/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INError.h"
#import "INInAppErrorCode.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppError : INError

@property (readwrite, nonatomic) INInAppErrorCode inAppErrorCode;

- (id)initWithErrorCode:(INInAppErrorCode)inAppErrorCode descriptionMessage:(nullable NSString *)descriptionMessage;

@end

NS_ASSUME_NONNULL_END
