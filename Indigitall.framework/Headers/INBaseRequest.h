//
//  BaseRequest.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "APIConstants.h"
NS_ASSUME_NONNULL_BEGIN

@interface INBaseRequest : NSObject{
    
@public
    NSString * bundleId;
    NSString * appKey;
    
@public
    NSMutableDictionary * body;
    NSString * urlParams;
}

@property (nonatomic, class, readonly) NSString * PARAMS_APP_KEY;
@property (nonatomic, class, readonly) NSString * PARAMS_ENABLED;
@property (nonatomic, class, readonly) NSString * PARAMS_PLATFORM;
@property (nonatomic, class, readonly) NSString * PARAMS_DEVICEID;
@property (nonatomic, class, readonly) NSString * PARAMS_TOPICS;
@property (nonatomic, class, readonly) NSString * PARAMS_EVENTTYPE;
@property (nonatomic,  class,readonly) NSString * PARAMS_PUSHID;
@property (nonatomic,  class,readonly) NSString * PARAMS_CLICKEDBUTTON;
@property (nonatomic, class, readonly) NSString * PARAMS_PERMISSIONTYPE;
@property (nonatomic, class, readonly) NSString * PARAMS_LATITUDE;
@property (nonatomic, class, readonly) NSString * PARAMS_LONGITUD;
@property (nonatomic, class, readonly) NSString * PARAMS_EXTERNAL_CODE;
@property (nonatomic, class, readonly) NSString * PARAMS_EXTENAL_APPS;
@property (nonatomic, class, readonly) NSString * PARAMS_DEVICE_TYPE;


@property (nonatomic, class, readonly) NSString * PARAMS_INAPP_ID;
@property (nonatomic, class, readonly) NSString * PARAMS_LAST_VERSION_ID;
@property (nonatomic, class, readonly) NSString * PARAMS_ACTION_ID;
@property (nonatomic, class, readonly) NSString * PARAMS_LIMIT;
@property (nonatomic, class, readonly) NSString * PARAMS_PAGE;
@property (nonatomic, class, readonly) NSString * PARAMS_INAPP_FORM;
@property (nonatomic, class, readonly) NSString * PARAMS_AUDIENCES;
@property (nonatomic, class, readonly) NSString * PARAMS_AUDIENCES_VERSION;

@property (nonatomic, class, readonly) NSString * PARAMS_INBOX_EXTERNALID;
@property (nonatomic, class, readonly) NSString * PARAMS_INBOX_PAGE;
@property (nonatomic, class, readonly) NSString * PARAMS_INBOX_PAGE_SIZE;
@property (nonatomic, class, readonly) NSString * PARAMS_INBOX_DATE_FROM;
@property (nonatomic, class, readonly) NSString * PARAMS_INBOX_DATE_TO;
@property (nonatomic, class, readonly) NSString * PARAMS_INBOX_STATUS;
@property (nonatomic, class, readonly) NSString * PARAMS_INBOX_INITIAL_STATUS;
@property (nonatomic, class, readonly) NSString * PARAMS_SENDER_ID;
@property (nonatomic, class, readonly) NSString * PARAMS_CAMPAIGN_ID;
@property (nonatomic, class, readonly) NSString * PARAMS_SENDER_IDS_LIST;
@property (nonatomic, class, readonly) NSString * PARAMS_INAPP_VERSION;
@property (nonatomic, class, readonly) NSString * PARAMS_INAPP_ACTION_ID;

@property (nonatomic) NSMutableURLRequest * headers;
@property (nonatomic, class, readonly) NSString * PARAMS_NETWORK_CONNECTED;
@property (nonatomic, class, readonly) NSString * PARAMS_NETWORK_SSID;
@property (nonatomic, class, readonly) NSString * PARAMS_DEVICE_SECURED_KEY;
@property (nonatomic, class, readonly) NSString * PARAMS_DEVICE_SECURED_DATA;
@property (nonatomic, class, readonly) NSString * PARAMS_EVENT_INBOX_EXTERNAL_CODE;
@property (nonatomic, class, readonly) NSString * PARAMS_EVENT_CUSTOM_DATA;
@property (nonatomic, class, readonly) NSString * PARAMS_APPLICATION_ID;

@property (nonatomic, class, readonly) NSString * PARAM_CUSTOMER_ID;
@property (nonatomic, class, readonly) NSString * PARAM_CUSTOMER_FIELD_NAMES;
@property (nonatomic, class, readonly) NSString * PARAM_CUSTOMER_FIELDS;
@property (nonatomic, class, readonly) NSString * PARAM_CUSTOMER_LINK;
@property (nonatomic, class, readonly) NSString * PARAM_CUSTOMER_CHANNEL;
@property (nonatomic, class, readonly) NSString * PARAM_JOURNEY_STATE_ID;
@property (nonatomic, class, readonly) NSString * PARAM_JOURNEY_EXECUTION;
@property (nonatomic, class, readonly) NSString * PARAM_CJ_CURRENT_STATE_ID;
@property (nonatomic, class, readonly) NSString * PARAMS_IS_PUSH_SECURE;


- (id) init;
- (NSString *) getParams;

- (NSMutableURLRequest *) addHeaders: (NSURL *)url;

@end

NS_ASSUME_NONNULL_END
