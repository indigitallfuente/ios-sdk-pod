//
//  CoreUtils.h
//  Indigitall
//
//  Created by indigitall on 2/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCoreUtils : NSObject

+ (NSDictionary *)toNSDictionary: (NSString *) string;
+ (NSString *) toNSString: (NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
