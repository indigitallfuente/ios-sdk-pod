//
//  InAppTopicUtils.h
//  inapp
//
//  Created by indigitall on 29/6/22.
//

#import <Foundation/Foundation.h>
#import "INInAppTopic.h"
#import "INInAppError.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppTopicUtils : NSObject
+ (void) saveTopics: (NSArray<NSString *>*) topics onSuccess: (nullable void(^)(NSArray<NSString *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;

+ (void) deleteTopics: (NSArray<NSString *>*) topics onSuccess: (nullable void(^)(NSArray<NSString *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;

+ (NSArray<NSString *>*) topicsToArray: (NSArray<INInAppTopic *> *)topics;

+ (NSArray<INInAppTopic *>*) stringArrayToTopics: (NSArray<NSString *> *)topics;

+ (NSString *)percentEscapeString:(NSString *)string;
@end

NS_ASSUME_NONNULL_END
