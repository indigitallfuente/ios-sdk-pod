//
//  INResponseInAppDevice.h
//  IndigitallSdk
//
//  Created by indigitall on 30/11/23.
//

#import <Foundation/Foundation.h>

#import "INBaseResponse.h"
#import "INInAppDevice.h"
NS_ASSUME_NONNULL_BEGIN

@interface INResponseInAppDevice : INBaseResponse

@property (nonatomic) INInAppDevice *device;

- (id) initWithCallback: (INBaseCallback *_Nullable) callback;
- (void) processWithData:(NSData *)data urlResponse:(NSURL *)urlResponse error:(NSError *)error;
@end

NS_ASSUME_NONNULL_END
