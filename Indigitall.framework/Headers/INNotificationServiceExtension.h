//
//  INNotificationServiceExtension.h
//  Indigitall
//
//  Created by indigitall on 07/10/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UserNotifications/UserNotifications.h>

NS_ASSUME_NONNULL_BEGIN

API_AVAILABLE(ios(10.0))
@interface INNotificationServiceExtension : NSObject

//@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
//@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;
//@property (nonatomic, nullable) UNNotificationRequest *receivedRequest;

+ (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler;
+ (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withBestAttempContent:(UNMutableNotificationContent *) bestAttemptContent withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler;
+ (void)serviceExtensionTimeWillExpire: (UNMutableNotificationContent *)bestAttemptContent  withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler;

@end

NS_ASSUME_NONNULL_END
