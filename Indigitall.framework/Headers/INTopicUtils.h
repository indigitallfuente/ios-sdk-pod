//
//  INTopicUtils.h
//  IndigitallSdk
//
//  Created by indigitall on 25/11/24.
//

#import <Foundation/Foundation.h>
#import "INInAppTopic.h"
#import "INTopic.h"

NS_ASSUME_NONNULL_BEGIN

@interface INTopicUtils : NSObject

+ (NSArray<NSString *>*) arrayStringFromInAppTopicArray: (NSArray<INInAppTopic *> *)inAppTopics;
+ (NSArray<INInAppTopic *>*) arrayInAppTopicFromStringArray: (NSArray<NSString *> *)topicCodes;

+ (NSArray<INTopic *> *) iNTopicArrayFromInAppTopicArray: (NSArray<INInAppTopic *> *) inAppTopics;
+ (NSArray<INInAppTopic *> *) inAppTopicArrayFromINTopicArray: (NSArray<INTopic *> *) topics;

+ (NSArray<NSString *>*) stringArrayFromINTopicArray: (NSArray<INTopic *> *)topics;
+ (NSArray<INTopic *>*) iNTopicArrayFromStringArray: (NSArray<NSString *> *)topicCodes  withChannel: (INChannel) channel;



@end

NS_ASSUME_NONNULL_END
