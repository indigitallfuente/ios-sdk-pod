//
//  Indigitall.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IndigitallPush.h"
#import "IndigitallCustomer.h"
#import "IndigitallCore.h"
#import "IndigitallInbox.h"
#import "IndigitallInApp.h"
#import "INDefaults.h"
#import "INTopicUtils.h"

NS_ASSUME_NONNULL_BEGIN

@interface Indigitall : NSObject


//Push

+ (void) initializeWithAppKey: (NSString *)appKey;
+ (void) initializeWithAppKey: (NSString *)appKey
      onIndigitallInitialized:(nullable void(^)( NSArray<INPermissions*> *permissions, INDevice *device))onIndigitallInitialized
      onErrorInitialized:(nullable void(^)(INError *onError))onErrorInitialized;
+ (void) initializeWithConfig: (INPushConfig *)config
      onIndigitallInitialized:(nullable void(^)(NSArray<INPermissions*> *permissions, INDevice *device))onIndigitallInitialized
      onErrorInitialized:(nullable void(^)(INError *onError))onErrorInitialized;

+ (void) getDeviceWithOnSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) enableDeviceWithOnSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) disableDeviceWithOnSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) setExternalCode: (NSString *) code onSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) logInWithId:(NSString *) code onSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) logOutWithSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;

+ (void) topicsListWithOnSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsSubscribeWithTopic: (NSArray<INTopic *>*)topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsSubscribeWithArrayString: (NSArray<NSString *>*) topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsUnSubscribeWithArrayString: (NSArray<NSString *>*)topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsUnSubscribeWithTopic: (NSArray<INTopic *>*)topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;

+ (void) askNotificationPermission;
+ (void) askLocationPermission;
+ (void) receivedPushWithCompletion: (void(^)(INPush * push, int identifier))completion;
+ (void) serviceDisabledWithCompletion:(void (^)(void))completion;

+ (void) setDeviceToken: (NSData *)deviceToken;
+ (void) setDeviceStringToken: (NSString *)pushToken onNewUserRegistered: (nullable void(^)(INDevice *device))onNewUserRegistered;
+ (void) setDeviceToken: (NSData *)deviceToken onNewUserRegistered: (nullable void(^)(INDevice *device))onNewUserRegistered;

+ (void) didReceivePushWithNotification:(NSDictionary *)data withCompletionHandler:(void(^)(INPush *push))completion;
+ (void) didReceiveWithNotification:(NSDictionary *)data withCompletionHandler:(void(^)(INPush *push))completion;

+ (void) handleWithNotification:(NSDictionary *)data identifier:(NSString *_Nullable)identifier DEPRECATED_ATTRIBUTE;
+ (void) handleWithNotification:(NSDictionary *)data identifier:(NSString *_Nullable)identifier withCompletionHandler:(nullable void(^)(INPush *push, INPushAction *action))completion DEPRECATED_ATTRIBUTE;
+ (void) handleWithResponse:(UNNotificationResponse *)response API_AVAILABLE(ios(10.0));
+ (void) handleWithResponse:(UNNotificationResponse *)response withCompletionHandler:(nullable void(^)(INPush *push,INPushAction *action))completion API_AVAILABLE(ios(10.0));
+ (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler API_AVAILABLE(ios(10.0));
+ (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withBestAttempContent:(UNMutableNotificationContent *) bestAttemptContent withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler;
+ (void)serviceExtensionTimeWillExpire: (UNMutableNotificationContent *)bestAttemptContent  withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler API_AVAILABLE(ios(10.0));
+ (UNNotificationPresentationOptions) willPresentNotification API_AVAILABLE(ios(10.0));
+ (void) handleWithData:(NSDictionary *)data identifier:(NSString *)identifier withCompletionHandler:(nullable void(^)(INPush *push, INPushAction *action))completion;

+ (void) performFetchWithCompletionHandler: (void(^)(UIBackgroundFetchResult handler))handler;
+ (void) registerDeviceForPush: (void(^)(INError *error,INDevice *device))completion;

+ (void) sendCustomEvent: (NSString *)eventCustom;
+ (void) sendCustomEvent: (NSString *)eventCustom customData:(nullable NSDictionary *)customData onSuccess:(nullable void(^)(void))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) registerStatistics:(UNNotificationResponse *)response API_AVAILABLE(ios(10.0));
+ (void) getLocation:(void(^)(NSString *latitude, NSString *longitude))location;

// InApp
+ (void) showInAppWithInAppId:(NSString *)inAppId view: (UIView *)view success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed;
+ (void) showInAppWithAppKey:(NSString *)appKey inAppId:(NSString *)inAppId view: (UIView *)view success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed;
+ (void) showInAppWithConfig:(INInAppConfig *)config InAppId: (NSString *)inAppId view: (UIView *)view didTouchWithAction:(nullable void(^)(INInAppAction *action))action onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed;
+ (void) showInAppWithConfig:(INInAppConfig *)config InAppId: (NSString *)inAppId view: (UIView *)view didTouchWithAction:(nullable void(^)(INInAppAction *action))action onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed formFailed:(nullable void(^)(NSArray<INInAppError*> *error))formFailed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

+ (void) showMultipleInAppWithListInAppId:(NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed;
+ (void) showMultipleInAppWithAppKey:(NSString *)appKey listInAppId:(NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed;
+ (void) showMultipleInAppWithConfig:(INInAppConfig *)config listInAppId:(NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView didTouch:(nullable void(^)(INInApp *inApp,UIView *webView, INInAppAction *action))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed;
+ (void) showMultipleInAppWithConfig:(INInAppConfig *)config listInAppId:(NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView didTouch:(nullable void(^)(INInApp *inApp,UIView *webView, INInAppAction *action))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed formFailed:(nullable void(^)(NSArray<INInAppError*> *error))formFailed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

+ (void) showPopupWithPopupId:(NSString *)popupId didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel failed:(nullable void(^)(INError *error))failed;
+ (void) showPopupWithAppKey:(NSString *)appKey popupId:(NSString *)popupId didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel failed:(nullable void(^)(INError *error))failed;
+ (void) showPopupWithConfig:(INInAppConfig *)config popupId:(NSString *)popupId closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel didTouchWithAction:(nullable void(^)(INInAppAction *action))action didDismissed:(nullable void(^)(void))didDismissed onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut didDismissForever:(nullable void(^)(INInApp *inApp, INError *error))didDismissForever failed:(nullable void(^)(INError *error))failed;
+ (void) showPopupWithConfig:(INInAppConfig *)config popupId:(NSString *)popupId closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel didTouchWithAction:(nullable void(^)(INInAppAction *action))action didDismissed:(nullable void(^)(void))didDismissed onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut didDismissForever:(nullable void(^)(INInApp *inApp, INError *error))didDismissForever failed:(nullable void(^)(INError *error))failed formFailed:(nullable void(^)(NSArray<INInAppError*> *error))formFailed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

// Inbox
+ (void) generateAuthTokenWithSuccess:(void(^)(NSString *token))onSuccess onError:(void(^)(INError *error))onError;
+ (void) getMessagesCountWithSuccess:(void(^)(INInboxCounters *counters))onSuccess onError:(void(^)(INError *error))onError;
+ (void) getInboxWithSuccess:(void(^)(INInbox *inbox))onSuccess onError:(void(^)(INError *error))onError;
+ (void) getInboxWithPage:(NSNumber * _Nullable)page pageSize:(NSNumber * _Nullable)pageSize dateFrom:(NSString * _Nullable)dateFrom dateTo:(NSString * _Nullable)dateTo statusList:(NSArray<NSString *> * _Nullable)statusList onSuccess:(void(^)(INInbox *inbox))onSuccess onError:(void(^)(INError *error))onError;

// Customer
+ (void) getCustomerWithSuccess: (void(^)(INCustomer *customer))onSuccess onError:(void(^)(INError *error))onError;
+ (void) getCustomerInformationWithFieldNames: (NSArray<NSString *> *_Nullable)fieldNames onSuccess:(void(^)(NSArray<INCustomerField *> *customerFields))onSuccess onError:(void(^)(INError *error))onError;
+ (void) assignOrUpdateValueToCustomerFieldsWithFields: (NSDictionary *)fields onSuccess:(void(^)(INCustomer *customer))onSuccess onError:(void(^)(INError *error))onError;
+ (void) deleteValuesFromCustomerFieldsWithFieldNames: (NSArray<NSString *> *)fieldNames onSuccess:(void(^)(INCustomer *customer))onSuccess onError:(void(^)(INError *error))onError;
+ (void) linkWithExtenalcode: (NSString *)code channel:(INChannel)channel onSuccess:(void(^)(void))onSuccess onError:(void(^)(INError *error))onError;
+ (void) unlinkWithChannel:(INChannel)channel  onSuccess:(void(^)(void))onSuccess onError:(void(^)(INError *error))onError;
+ (void) sendCustomerCustomEvent: (NSDictionary *)customData eventCode: (NSString *) eventCode onSuccess:(void(^)(INCustomer *costumer))onSuccess onError:(void(^)(INError *error))onError;

@end

NS_ASSUME_NONNULL_END
