//
//  INBaseApplicationRequest.h
//  IndigitallSdk
//
//  Created by indigitall on 14/2/24.
//

#import <Foundation/Foundation.h>
#import "INBaseRequest.h"
#import "INChannel.h"

NS_ASSUME_NONNULL_BEGIN

@interface INBaseApplicationRequest :  INBaseRequest

@property (nonatomic) NSArray *channels;

-(id) initWithChannels: (NSArray *)channels;
- (id) getApplicationRequest;

@end

NS_ASSUME_NONNULL_END
