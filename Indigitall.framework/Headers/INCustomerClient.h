//
//  CustomerClient.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INCustomerRequest.h"
#import "INBaseClient.h"
#import "INCustomerCallback.h"
#import "INCustomerFieldCallback.h"
#import "INCustomerLinkCallback.h"

NS_ASSUME_NONNULL_BEGIN

@interface INCustomerClient : INBaseClient

+ (void) getCustomer: (INCustomerRequest *)request completion: (INCustomerCallback *)callback;
+ (void) getCustomerField: (INCustomerRequest *)request completion: (INCustomerFieldCallback *)callback;
+ (void) putCustomerField: (INCustomerRequest *)request completion: (INCustomerCallback *)callback;
+ (void) deleteCustomerField: (INCustomerRequest *)request completion: (INCustomerCallback *)callback;
+ (void) postCustomerLink: (INCustomerRequest *)request completion: (INCustomerLinkCallback *)callback;
+ (void) deleteCustomerLink: (INCustomerRequest *)request completion: (INCustomerLinkCallback *)callback;
+ (void) postCustomJourneyEvent: (INCustomerRequest *)request completion: (INCustomerCallback *)callback;

@end

NS_ASSUME_NONNULL_END
