//
//  CustomerCallback.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseCallback.h"
#import "INCustomer.h"

NS_ASSUME_NONNULL_BEGIN

@interface INCustomerCallback : INBaseCallback

@property (nonatomic) INCustomer *customer;

typedef void(^onErrorCustomer)(INError * error);
@property (readwrite, copy) onErrorCustomer onError;

typedef void(^onSuccessCustomer)(INCustomer *customer);
@property (readwrite, copy) onSuccessCustomer onSuccess;

- (id)initWithOnSuccess: (void(^)(INCustomer *customer))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;



@end

NS_ASSUME_NONNULL_END
