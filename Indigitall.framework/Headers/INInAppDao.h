//
//  InAppDao.h
//  Indigitall
//
//  Created by indigitall on 10/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInApp.h"
#import "INDBContentProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppDao : NSObject

+ (NSArray *)setContentValue: (INInApp *)inApp;

+ (BOOL) addNewInApp: (INInApp *) inApp;
+ (BOOL) deleteInApp: (int) inAppId;
+ (BOOL) deleteInAppWithOldVersionWithInAppId: (int) inAppId withVersion: (int)inAppVersion;
+ (BOOL) clearInAppDatabase;
+ (BOOL) updateInAppField: (INInApp *) inApp key:(NSString *)key value:(NSObject *)value ;
+ (BOOL) updateInAppField: (INInApp *) inApp keys:( NSArray<NSString *>* _Nullable )keys values:(NSArray<NSObject *>* _Nullable)values;
+ (BOOL) updateInAppField: (INInApp *) inApp withInAppDb:(INInApp * _Nullable) inAppDb keys:( NSArray<NSString *>* _Nullable )keys values:(NSArray<NSObject *>* _Nullable)values;
+ (INInApp *) searchInApp: (NSString *) inAppCode;
+ (INInApp *) searchInAppWithInAppId: (int) inAppId;
@end

NS_ASSUME_NONNULL_END
