//
//  DBContentProvider.h
//  Indigitall
//
//  Created by indigitall on 9/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INDBContentProvider : NSObject

+ (char *) insertRowWithTableName: (NSString *)tableName args:(NSArray *) args values: (NSArray *) values;
+ (char *) clearTableWithName: (NSString *)tableName;
+ (char *) deleteRowWithTableName: (NSString *)tableName selection: (NSString *)selection condition: (int) condition;
+ (char *) deleteRowCompareVersionWithTableName: (NSString *)tableName selection: (NSString *)selection condition: (int) condition version: (int) version;
+ (char *) editRowWithTableName: (NSString *)tableName selectionKeys: (NSArray<NSString *>*)selectionKeys selectionValues: (NSArray<NSObject *>*)selectionValues conditionKey: (NSString *) conditionKey conditionValue:(NSObject *)conditionValue;
+ (char *) findRowWithTableName: (NSString *)tableName selectionKeys: (NSArray *)selectionKeys conditionKey:(NSString *)conditionKey conditionValue: (NSString *) conditionValue;
+ (char *) findRowWithTableName: (NSString *)tableName selectionKeys: (NSArray *)selectionKeys conditionKey:(NSString *)conditionKey conditionIntValue: (int) conditionValue;

@end

NS_ASSUME_NONNULL_END
