//
//  PushValidations.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INValidations.h"
NS_ASSUME_NONNULL_BEGIN

@interface INPushValidations : INValidations
+ (BOOL) isValidFormatRequest;
@end

NS_ASSUME_NONNULL_END
