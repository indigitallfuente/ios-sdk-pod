//
//  KeyChainService.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INKeyChainService : NSObject
- (OSStatus)save: (NSString *)key data: (NSData *)data;
- (NSData *)load: (NSString *)key;
- (NSString *)createUniqueID;

@end

NS_ASSUME_NONNULL_END
