//
//  INInAppAudience.h
//  IndigitallSdk
//
//  Created by indigitall on 15/11/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppAudience : NSObject

@property (nonatomic) NSArray<NSString *>* audiences;
@property (nonatomic) NSString *lastUpdate;
+ (int) CACHE_TTL_DEFAULT;

- (id) initWithJsonString: (NSString *) jsonString;
- (id) initWithJson: (NSDictionary *) json;
- (NSString *) toString;
- (NSDictionary *) toJson;

@end

NS_ASSUME_NONNULL_END
