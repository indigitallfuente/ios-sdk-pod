//
//  InboxAuthenticationRequest.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxAuthenticationRequest : INBaseRequest

@property (nonatomic) NSDictionary *json;
- (id) init;
- (id) postInboxAuth;


@end

NS_ASSUME_NONNULL_END
