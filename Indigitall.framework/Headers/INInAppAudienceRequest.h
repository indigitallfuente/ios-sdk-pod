//
//  INRequestInAppAudiences.h
//  IndigitallSdk
//
//  Created by indigitall on 15/11/24.
//

#import <Foundation/Foundation.h>

#import "INBaseRequest.h"
NS_ASSUME_NONNULL_BEGIN

@interface INInAppAudienceRequest : INBaseRequest


-(id) init;

@end

NS_ASSUME_NONNULL_END
