//
//  FormRequestInApp.h
//  IndigitallInApp
//
//  Created by indigitall on 22/12/22.
//

#import <Foundation/Foundation.h>
#import "INInApp.h"
#import "INBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface INFormRequestInApp : INBaseRequest

@property (nonatomic) NSString *deviceId;
@property (nonatomic) NSDictionary *form;
@property (nonatomic) NSString *externalId;
@property (nonatomic) INInApp *inApp;

- (id) initWithInApp: (INInApp *)inApp jsonForm:(NSDictionary *)jsonForm;
- (id) postForm;

@end


NS_ASSUME_NONNULL_END
