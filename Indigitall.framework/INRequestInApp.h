//
//  RequestInApp.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseRequest.h"
#import "INInAppDevice.h"
#import "INInAppAudience.h"
NS_ASSUME_NONNULL_BEGIN

@interface INRequestInApp : INBaseRequest

@property (nonatomic) NSString *inAppId;
@property (nonatomic) NSString *latitude;
@property (nonatomic) NSString *longitude;
@property (nonatomic) NSArray *topics;
@property (nonatomic) INInAppAudience *audiences;


-(id) initWithInAppId: (NSString *)inAppId device: (INInAppDevice *)device;
//-(id) initWithInAppId: (NSString *)inAppId;
@end


NS_ASSUME_NONNULL_END
