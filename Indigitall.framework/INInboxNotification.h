//
//  InboxNotification.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInboxPush.h"
#import "INInboxStatus.h"
#import "INInboxCategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxNotification : NSObject


@property (nonatomic) NSString *idInboxNotification;
@property (nonatomic) NSString *externalId;
@property (nonatomic) NSString *sentAt;
@property (nonatomic) INInboxStatus *inboxStatus;
@property (nonatomic) NSNumber *sendingId;
@property (nonatomic) NSNumber *campaignId;
@property (nonatomic) INInboxPush *message;
@property (nonatomic) NSString *replacements;
@property (nonatomic) Boolean read;
@property (nonatomic) INInboxCategory *category;

- (id) init;
- (id) initWithJson: (NSDictionary *)json;
- (id) initWithJson: (NSDictionary *)json lastAccess: (NSString *)lastAccess;
@end

NS_ASSUME_NONNULL_END
