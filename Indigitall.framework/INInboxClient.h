//
//  InboxClient.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInboxAuthenticationRequest.h"
#import "INInboxCountersRequest.h"
#import "INInboxPushRequest.h"
#import "INBaseClient.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxClient : INBaseClient


+ (void) getInbox: (INInboxPushRequest *) request completion: (INBaseCallback *)callback;
+ (void) putInboxPush: (INInboxPushRequest *) request completion: (INBaseCallback *)callback;
+ (void) getInboxPushWithSendingId: (INInboxPushRequest *) request completion: (INBaseCallback *)callback;
+ (void) putInboxPushwithSendingId: (INInboxPushRequest *) request completion: (INBaseCallback *)callback;
+ (void) getInboxCounter: (INInboxCountersRequest *) request completion: (INBaseCallback *)callback;
+ (void) postInboxAuth: (INInboxAuthenticationRequest *) request completion: (INBaseCallback *)callback;

@end

NS_ASSUME_NONNULL_END
