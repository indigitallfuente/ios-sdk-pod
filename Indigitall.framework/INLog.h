//
//  INLog.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INError.h"
NS_ASSUME_NONNULL_BEGIN

@interface INLog : NSObject
@property (nonatomic) int debug;
@property (nonatomic) int info;
@property (nonatomic) int warning;
@property (nonatomic) int error;

@property (nonatomic) NSString* tag;
@property (nonatomic) NSString* log;
@property (nonatomic) int level;

- (id)init: (NSString *)tag;
- (void)Log: (NSString *)tag level:(int) level;

- (void) writelog;

- (void)d: (NSString *)message;
- (void)i: (NSString *)message;
- (void)w: (NSString *)message;
- (void)e: (NSString *)message;
- (void)error: (INError *)message;
- (void)setLevelLog:(int)newLevel;
@end

NS_ASSUME_NONNULL_END
