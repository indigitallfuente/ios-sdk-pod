//
//  InAppConfig.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInAppLocationHandler.h"
#import "INInAppDefaults.h"
#import "INCoreConfig.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppConfig : INCoreConfig

@property (nonatomic) INInAppConfig * inAppCurrent;
@property (nonatomic) NSString * domainInApp;
@property (nonatomic) BOOL locationEnabled;
@property (nonatomic) BOOL closePopupWhenClicked;

+ (id) current;

- (id) init;
- (id) initWithAppKey:(NSString *) appKey;
- (id) initWithJSON: (NSDictionary *)config;
- (void) updateConfig ;
@end

NS_ASSUME_NONNULL_END
